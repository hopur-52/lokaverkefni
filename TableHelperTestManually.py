from View.Helpers.TableHelper import Table
# Test input 1
column_names1 = ['name','kennitala','phone','cc']
column_items1 = {'pall':[2812932229,6167821,('xxxx-xxxx-xxxx-xxxx','10/21', 466)],'jon':[6666661337,5812345,('zzzz-zzzz-zzzz-zzzz', '09/19', 488)]}
# Test input 2
car_columnnames1 = ['plate','price category','price','car type','brand','color','fuel']

car_colum_items1 = {'AB123':[13500,15000,'4x4','land rover','white','oct95'],'RE652':[14000,14353,'normal','yaris','orange','oct95']}

c = Table(car_columnnames1, car_colum_items1,'Cars')
print(c.print_table())