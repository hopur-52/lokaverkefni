from Data.Helpers.DateTime import DateTimeHelper as dt
from datetime import datetime
from Services.OrderService import OrderService

class CarService:

    @staticmethod
    def confirm_car_return(car ='',orders_dict = {}):
        LATE_FEE=17000
        orders_arr=[]
        for key,value in orders_dict.items():
            for key1,lease in value.leases.items():
                if lease.car == str(car):
                    orders_arr.append(key)
        
        order_id = orders_arr[-1]
        order_info = orders_dict[str(order_id)]
        for i,j in order_info.leases.items():
            if datetime.strptime(j.lease_end,'%Y-%m-%d %H:%M:%S') < datetime.now(): #Athuga hvort skil sein
                return_delay = int(str(datetime.now()-datetime.strptime(j.lease_end,'%Y-%m-%d %H:%M:%S'))[0])
                Total_late_fee = LATE_FEE*return_delay
                orders_dict = OrderService.update_order_price(order_id,orders_dict)
                orders_dict[str(order_id)].price_w_VAT =str(orders_dict[str(order_id)].price_w_VAT) +" + Late Fee of total {} kr".format(Total_late_fee)
                j.lease_end = datetime.now()
            elif datetime.strptime(j.lease_end,'%Y-%m-%d %H:%M:%S') > datetime.now():
                j.lease_end = datetime.now()

        return orders_dict

        

    @staticmethod
    def get_unavailable_cars(start_time=dt.now(),end_time=dt.now(),cars={},orders = {}):
        if type(start_time) is type(str()):
            start_time = dt.stringToDateTime(start_time)
        if type(end_time) is type(str()):
            end_time = dt.stringToDateTime(end_time)

        unavailable_cars = set()
        for key,value in orders.items():
            value_arr = str(value).split('|')
            lease_arr = value_arr[4].split('[-^-]')
            for lease in lease_arr:
                single_lease_arr = lease.split('///')
                lease_start = dt.stringToDateTime(single_lease_arr[1])
                lease_end = dt.stringToDateTime(single_lease_arr[2].split('.')[0])
                if (lease_start <= start_time <= lease_end) or (lease_start <= end_time <= lease_end) or (start_time <= lease_start <= end_time) or (start_time <= lease_end <= end_time):
                    unavailable_cars.add(single_lease_arr[3])

        return unavailable_cars

    @staticmethod
    def get_available_cars(start_time=dt.now(),end_time=dt.now(),cars={},orders = {}):
        if type(start_time) is type(str()):
            start_time = dt.stringToDateTime(start_time)
        if type(end_time) is type(str()):
            end_time = dt.stringToDateTime(end_time)

        unavailable_cars = set() #Set of unavailable cars during input time range.
        all_cars = set() #Set of all rental cars of the company
        for key,value in orders.items():
            value_arr = str(value).split('|')
            lease_arr = value_arr[4].split('[-^-]')
            for lease in lease_arr:
                single_lease_arr = lease.split('///')
                lease_start = dt.stringToDateTime(single_lease_arr[1])
                lease_end = dt.stringToDateTime(single_lease_arr[2].split('.')[0])

                if (lease_start <= start_time <= lease_end) or (lease_start <= end_time <= lease_end) or (start_time <= lease_start <= end_time) or (start_time <= lease_end <= end_time):
                    unavailable_cars.add(single_lease_arr[3])

        for key,value in cars.items():
            all_cars.add(key)

        return all_cars - unavailable_cars
