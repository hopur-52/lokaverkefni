import unittest
from Data.Models.CarModel import CarModel
from Data.Models.OrderModel import OrderModel
from Services.CarService import CarService
from Data.Helpers.DateTime import DateTimeHelper as dt

class CreateCarView_test(unittest.TestCase):

    def setUp(self):
        self.cars = self.__init_cars_test_data()
        self.orders = self.__init_orders_test_data()

    def __init_cars_test_data(self):
        cars = {}
        cars['FD-234'] = CarModel('FD-234', '55000','1500', 'Fólksbíll', 'Honda', 'Grár', 'Bensín')
        cars['XB-305'] = CarModel('XB-305', '55000','1500', 'Fólksbíll', 'Honda', 'Rauður', 'Bensín')
        cars['XY-567'] = CarModel('XY-567','69000','1500', 'Jeppi', 'Toyota Cruiser', 'Svartur','Dísel')

        return cars

    def __init_orders_test_data(self):
        orders = {}
        orders[1234] = OrderModel(1234,2805922859,1234,'card','1///2018-12-08 19:59:16///2018-12-18 19:59:16///FD-2341///34445///y///4000|25000|30000|2018-11-18 19:59:16')
        orders[1244] = OrderModel(1244,2805922859,1234,'card','1///2018-12-02 19:59:16///2018-12-15 19:59:16///XY-567///34445///y///4000[-^-]2///2018-12-16 19:59:16///2018-12-19 19:59:16///XB-305///34445///y///4000|25000|30000|2018-11-18 19:59:16')
        return orders

    def test_get_available_cars(self):
        # Test should return 3 available cars
        a_cars_avail = CarService.get_available_cars("2018-12-23 19:59:16",dt.stringToDateTime("2019-01-11 19:59:16"),self.cars,self.orders)
        self.assertEqual(len(a_cars_avail), 3, 'Should return 3 available cars')

        # Test should return 1 available cars
        b_cars_avail = CarService.get_available_cars(dt.stringToDateTime("2018-12-08 19:59:16"),dt.stringToDateTime("2018-12-15 19:59:16"),self.cars,self.orders)
        print(b_cars_avail)
        self.assertEqual(len(b_cars_avail), 1, 'Should return 1 available car')


    def test_get_unavailable_cars(self):
        # Test should return 2 unavailable cars
        a_cars = CarService.get_unavailable_cars(dt.stringToDateTime("2018-12-08 19:59:16"),dt.stringToDateTime("2018-12-14 19:59:16"),self.cars,self.orders)
        self.assertEqual(len(a_cars), 2, 'Should return two unavailable cars')

        # Test should return 3 unavailable cars
        b_cars = CarService.get_unavailable_cars(dt.stringToDateTime("2018-12-26 19:59:16"),dt.stringToDateTime("2019-1-05 19:59:16"),self.cars,self.orders)
        self.assertEqual(len(b_cars), 0, 'Should return 0 unavailable cars')



if __name__ == '__main__':
    unittest.main()
