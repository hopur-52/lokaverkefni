from View.Inputs.Customer.UpdateCustomerSelect import UpdateCustomerInput
from View.Inputs.Customer.KennitalaInput import KennitalaInput
from View.Inputs.NameInput import NameInput
from Data.Models.CustomerModel import CustomerModel
from View.Inputs.Customer.PhoneNumberInput import PhoneNumberInput
from View.Inputs.Customer.CreditCardInput import CreditCardInput
from View.Inputs.Customer.AddressInput import AddressInput


class CustomerService:
    @staticmethod
    def find_one_or_fail(kennitala=None, customers={}):
        if kennitala in customers.keys():
            return customers[kennitala]
        else:
            raise LookupError("Kennitala fannst ekki")


    @staticmethod
    def delete_customer(customer_id,customer_dict):
        pass
        
    @staticmethod
    def create_address():
        street = AddressInput.street_name_input_field()
        house = AddressInput.house_number_input_field()
        city = AddressInput.city_input_field()
        country = AddressInput.country_input_field()
        return "{}///{}///{}///{}".format(street, house, city, country)

    @staticmethod
    def create_card():
        return CreditCardInput.create_card()


    @staticmethod
    def register_customer(customer_dict = {}): #Skrá nýjan viðskiptavin
        print("\033[1;34;40m\t\tCreate a new customer\n")
        kennitala = KennitalaInput.input_field()
        if kennitala in customer_dict.keys():
            print("Kennitala already in use.")
            return False
        else:
            name = NameInput.input_field()
            address = CustomerService.create_address()
            phone = PhoneNumberInput.input_field()
            card = CustomerService.create_card()
            return CustomerModel(name,kennitala,address,phone,'',card)

    @staticmethod
    def update_customer_info(customer_id,customer_dict,order_dict):
        customer_copy = customer_dict[str(customer_id)]
        choice = UpdateCustomerInput.choose_option()

        if choice == '1':
            customer_dict = CustomerService.edit_kennitala(customer_id,customer_dict)
        elif choice == '2':
            customer_dict = CustomerService.edit_name(customer_id,customer_dict)
        elif choice == '3':
            customer_dict = CustomerService.edit_address(customer_id,customer_dict)
        elif choice == '4':
            customer_dict = CustomerService.edit_phone(customer_id,customer_dict)
        elif choice == '5':
            customer_dict = CustomerService.edit_card(customer_id,customer_dict)
        elif choice == '6':
            customer_dict = CustomerService.get_customer_orders(customer_id,order_dict)

        return customer_dict
       
    @staticmethod
    def get_customer_orders(customer_id,order_dict):
        customer_orders = {}
        for key,value in order_dict.items():
            if value.customer_id == customer_id:
                customer_orders[key] = value
        return customer_orders



    @staticmethod
    def edit_kennitala(customer_id, customer_dict):
        new_kennitala = KennitalaInput.input_field()
        customer_dict[str(customer_id)].kennitala = new_kennitala
        customer_dict[new_kennitala] = customer_dict[str(customer_id)]
        del customer_dict[str(customer_id)]
        return customer_dict

    @staticmethod
    def edit_name(customer_id,customer_dict):
        new_name = input("Enter new name.")
        customer_dict[str(customer_id)].name = new_name
        return customer_dict

    @staticmethod
    def edit_address(customer_id,customer_dict):
        new_address = CustomerService.create_address()
        customer_dict[str(customer_id)].address = new_address
        return customer_dict 

    @staticmethod
    def edit_phone(customer_id,customer_dict):
        new_phone = PhoneNumberInput.input_field()
        customer_dict[str(customer_id)].phone = new_phone
        return customer_dict

    @staticmethod
    def edit_card(customer_id,customer_dict):
        choice = input("1. Add card\n2. Remove card\n3. Back\nSelect action: ")
        while choice not in ['1','2','3']:
            print('Invalid choice, try again.')
            choice = input("1. Add card\n2. Remove card\n3. Back\nSelect action: ")

        if choice == '1':
            customer_dict=CustomerService.add_card(customer_id,customer_dict)
            return customer_dict
        elif choice == '2':
            customer_dict=CustomerService.remove_card(customer_id,customer_dict)
            return customer_dict
        elif choice == '3':
            return CustomerService.update_customer_info(customer_id,customer_dict)
        
    @staticmethod
    def add_card(customer_id,customer_dict):
        card_number,new_card = CustomerService.create_card()
        customer_dict[str(customer_id)].cards[str(card_number)] = new_card
        return customer_dict

    @staticmethod
    def remove_card(customer_id,customer_dict):
        for key in customer_dict[str(customer_id)].cards.keys():
            print(key)
        
        inp = input("Input card number you wish to remove xxxxxxxxxxxxxxxx: \nPress q to quit")
        
        while inp not in customer_dict[str(customer_id)].cards.keys() and inp != 'q':
            print("Card not found.")
            inp = input("Input card number you wish to remove xxxxxxxxxxxxxxxx: \nPress q to quit")

        if inp == 'q':
            return customer_dict
        else:
            del customer_dict[str(customer_id)].cards[inp]
            return customer_dict
