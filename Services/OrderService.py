from Data.Models.OrderModel import OrderModel
from Data.Models.OrderModel import Lease
from Data.Helpers.Crypt import Crypt
from Data.Validators.OrderValidator import OrderValidator
from View.Inputs.Order.BillingTypeSelect import BillingTypeInput
from View.Inputs.Order.EditOrderSelect import EditOrderInput
from View.Inputs.Car.CarLicensePlateInput import CarLicensePlaceInput
from View.Inputs.DateTimeInput import DateTimeInput
from Data.Helpers.DateTime import DateTimeHelper as dth

class OrderService:

    @staticmethod
    def update_order_price(order_id,order_dict):
        #updated_order_price() er notað i add_lease og remove_lease
        #til að uppfaera verd i samraemi vid breytingar.

        #calculate new price
        updated_price = 0
        for key,lease in order_dict[order_id].leases.items():
            updated_price += int(lease.price_total)
        updated_price_with_VAT = int(updated_price) * int(1.11)

        order_dict[order_id].price_total = int(updated_price)
        order_dict[order_id].price_w_VAT = int(updated_price_with_VAT)
        return order_dict

    @staticmethod
    def add_lease(order_number,order_dict,cars_dict):
        lid = 1
        for key,value in order_dict[order_number].leases.items():
            lid += 1
        order_dict[order_number].leases[lid] = OrderService.create_lease(lid,cars_dict,order_dict)
        order_dict = OrderService.update_order_price(order_number,order_dict)

        return order_dict #returns updated version of order dictonary

    @staticmethod
    def remove_lease(order_id,order_dict):
        order_dict[order_id]
        for key,value in order_dict[order_id].leases.items():
            print(value)
        inp = input("Please enter int for order you wish to remove.")

        while str(inp) not in order_dict[order_id].leases:
            print("Could not find lease, please try again. ")
            inp = input("Please enter int for order you wish to remove.")
        
        del order_dict[order_id].leases[inp]

        order_dict = OrderService.update_order_price(order_id,order_dict)

        return order_dict #returns updated version of order dictonary

    @staticmethod
    def edit_order(order_number,order_dict,cars_dict):
        choice = EditOrderInput.choose_action()
        if choice == '1':
             order_dict =OrderService.add_lease(order_number,order_dict,cars_dict)
        if choice == '1':
            order_dict = OrderService.remove_lease(order_number,order_dict)
        return order_dict


    @staticmethod
    def delete_order(order_number,order_dict):
        pass

    @staticmethod
    def create_lease(lid,cars_dict,orders_dict,car_plate=''):
        lid = lid
        lease_start = DateTimeInput.input_future()
        lease_end = DateTimeInput.input_future()

        if car_plate != '':
            # Get available cars for that time period
            from Services.CarService import CarService
            avail_cars = CarService.get_available_cars(lease_start,lease_end,cars_dict,orders_dict)

            # Return that to a table select
            from View.MenuViews.CarViews.SelectCarView import SelectCarView
            car_table = SelectCarView(cars_dict,orders_dict)

            # When selected from table it becomes the car variable
            car = car_table.Menu()
        else:
            car = car_plate
        rental_length = (lease_end-lease_start).days #Duration of rental period in days.

        license_plate = car.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.
        price = (cars_dict[license_plate].price * rental_length)
        extra_insurance=input("Extra insurane, y/n ?").lower()
        price_total = 0
        if extra_insurance == 'n':
            price_total = price
        else:
            price_total = price + (cars_dict[license_plate].extra_ins_price * rental_length)

        lease = Lease(lid,lease_start,lease_end,price,extra_insurance,price_total)

        return lease #Returnar instance af Lease klasanum.

    @staticmethod
    def create_order(cars_dict,orders_dict,car_plate=""):
        VAT = 1.11
        order_id = Crypt.generate_id()

        employe_id = input("Type in employee ID: ") #Prompt for Employee ID

        customer_id = input("Type in customer ID: ") #Prompt for Customer ID 

        billing_type = BillingTypeInput.select_billing()
        leases =  {}
        price = 0
        for key,lease in leases.items():
            price += int(lease.price_total)

        price_with_VAT = price * VAT
        lid = 1
        add_another_lease = "y"
        while add_another_lease == 'y':
            lease = OrderService.create_lease(lid,cars_dict,orders_dict,car_plate)
            leases[lid]=lease
            add_another_lease = input("Add another lease, y/n ? ")
            lid += 1

        order = OrderModel(order_id,customer_id,employe_id,billing_type,leases,price,price_with_VAT)
        orders_dict[order_id] = order
        return orders_dict # Returnar dict af update orders
