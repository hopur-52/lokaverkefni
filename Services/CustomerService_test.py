import unittest
from Data.Models.CustomerModel import CustomerModel as cm
from Services.CustomerService import CustomerService as cs

class CustomerServiceTest(unittest.TestCase):
    def setUp(self):
        customers = {}
        customers[2805922859] = cm('Einar', '2805922859', 'Snorrabraut///13///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1234123412341234///123///4-23[-^-]1234123412341234///321///5-20')
        customers[1234123482] = cm('Palli', '1234123412', 'Menntavegur///1///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1234123412341234///123///4-23')

        self.customers = customers

    def test_find_one_or_fail(self):
        # Should return  a customer object
        test1 = cs.find_one_or_fail(2805922859, self.customers)
        self.assertIsInstance(test1, cm)

        # Should raise a lookup error
        with self.assertRaises(LookupError) as test2:
            cs.find_one_or_fail(2805922853, self.customers)
            exception = test2.exception
            self.assertEqual(exception, "Kennitala fannst ekki")


    def test_register_customer(self):
        cs.register_customer()
