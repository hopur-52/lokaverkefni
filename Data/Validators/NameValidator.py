class NameValidator:
    @staticmethod
    def validate_name_input(inp):
        errors = []
        # Put validation logic here
        if len(inp) > 60:
            errors.append("Name must be less than 60 charcters!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
