"""
    Work in progress, not everything implemented yet.
"""
class CarColorValidator:

    @staticmethod
    def validate_car_color_input(inp):
        errors = []
        # Put validation logic here

        # Cant be longer then 15 characters
        if str(inp).__len__() > 15:
            errors.append("Color can't be longer then 15 characters")

        # End of validation logic
        response = {}

        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
