class LicensePlateValidator:

    @staticmethod
    def validate_license_plate_in_dict(license_plate,cars={}):

        errors = []
        # Put validation logic here
        if len(license_plate) > 15:
            errors.append("license plate can't be more than 15 characters!")
        # Validate if it exists in dictionary
        if license_plate not in cars:
            errors.append("Could not find in database")

        # End of validation logic

        response = {}

        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"
            response["car"] = cars[license_plate]

        return response
