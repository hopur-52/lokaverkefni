class CarBrandValidator:

    @staticmethod
    def validate_car_brand_input(inp):
        errors = []
        # Put validation logic here

        if len(inp) > 50:
            errors.append("Car brand can't be more than 50 characters!")


        # End of validation logic
        response = {}

        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
