class AddressValidator:
    @staticmethod
    def validate_address_street_name_input(inp):
        errors = []
        # Put validation logic here

        if not str(inp).isalpha():
            errors.append("Street name can only contain letters")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    @staticmethod
    def validate_address_house_number_input(inp):
        errors = []
        # Put validation logic here

        if not str(inp).isdigit():
            errors.append("House number must be a digit")

        if len(inp) > 6:
            errors.append("House number can't be more than 6 numbers!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    @staticmethod
    def validate_address_city_input(inp):
        errors = []
        # Put validation logic here
        if len(inp) > 50:
            errors.append("City can't be longer than 50 characters!")

        if str(inp).isdigit():
            errors.append("City can't contain a number!")


        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    @staticmethod
    def validate_address_country_input(inp):
        errors = []
        # Put validation logic here
        if str(inp).isdigit():
            errors.append("Country can't contain a digit!")
        if str(inp).__len__() > 20:
            errors.append("Country can't be more than 20 characters!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
