class PhoneValidator:
    @staticmethod
    def validate_phone_input(inp):
        errors = []
        # Put validation logic here

        # Must be 7 characters long
        if str(inp).__len__() is not 7:
            errors.append("Phone number must be 7 characters long")
        # Must only be digits
        if str(inp).isdigit() == False:
            errors.append("Phone number must only contain digits")


        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
