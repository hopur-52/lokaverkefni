class KennitalaValidator:
    @staticmethod
    def validate_kennitala_input(inp):
        errors = []

        if str(inp).__len__() != 10:
            errors.append("Kennitala must be 10 digits")

        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
