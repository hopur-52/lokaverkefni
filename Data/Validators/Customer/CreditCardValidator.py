class CreditCardValidator:
    @staticmethod
    def validate_CreditCard_input_cardnumber(inp):
        errors = []
        # Put validation logic here

        if str(inp).isalpha():
            errors.append("Car number can't contain a character!")

        if len(inp) > 19:
            errors.append("Credit card number can't be more than 19 digits!")


        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    def validate_CreditCard_input_ccv(inp):
        errors = []
        # Put validation logic here
        if str(inp).isalpha():
            errors.append("CCV have to be digits!")

        if len(inp) > 4:
            errors.append("CCV can't be more than 4 digits")


        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    def validate_CreditCard_input_monthyear(inp):
        errors = []
        # Put validation logic here
        if str(inp).isalpha():
            errors.append("month year can't be a character!")

        if len(inp) > 5:
            errors.append("Year can't be more than four digits!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
