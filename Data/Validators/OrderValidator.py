from datetime import datetime
from Data.Helpers.DateTime import DateTimeHelper as dt
#from View.Exceptions.LesserDateException import LesserDateException
from View.Inputs.DateTimeInput import DateTimeInput as dt_inp

class OrderValidator:

    @staticmethod
    def validate_rental_period():
        #lease_start = dt_inp.input_future()
        
        lease_start = input("Input start of period time in format: 'yyyy-mm-dd 15:00:00' ")
        try:
            lease_start = dt.stringToDateTime(lease_start)
        except:
            print("Invalid format, try again.")
            #lease_start = dt_inp.input_future()
            #lease_start = dt.stringToDateTime(lease_start)


        while lease_start < dt.now():
            print("Error, inserted date is lesser then current date, please insert valid date")
            #lease_start = dt_inp.input_future()
            lease_start = dt.stringToDateTime(lease_start)
            
            
            #lease_end = dt_inp.input_future()
        lease_end = input("Input end of period time in format: 'yyyy-mm-dd 15:00:00' ")

        try:
            lease_end = dt.stringToDateTime(lease_end)
        except:
            print("Invalid format, try again.")
            #lease_start = dt_inp.input_future()
            lease_start = dt.stringToDateTime(lease_start)

        while lease_start >= lease_end:
            print("End date of rental period is lesser then start of rental period.")
            lease_end = input("Input end of period time in format: 'yyyy-mm-dd 15:00:00' ")
            lease_end = datetime.strptime(lease_end, '%Y-%m-%d %H:%M:%S')
        
        return lease_start, lease_end

