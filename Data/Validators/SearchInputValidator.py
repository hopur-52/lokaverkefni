import string

class SearchInputValidator:
    @staticmethod
    def validate_search_input(inp):
        # Input to lower
        inp = inp.lower()
        errors = []
        # Put validation logic here
        if len(inp) > 100:
            errors.append("Input must be less than 100 characters!")

        # Must only contain lowercase, uppercase, numbers, spaces and dashes
        for char in inp:
            if char.isalpha() == False and char.isdigit() == False:
                if char != ' ' and char != '-':
                    errors.append("Must only contain lowercase, uppercase, numbers, spaces and dashes")
                    break


        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
