class EmployeeValidator:
    @staticmethod
    def validate_Employee_input_name(inp):
        errors = []
        # Put validation logic here
        if len(inp) > 60:
            errors.append("Employee name length can't be more than 60 characters!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    @staticmethod
    def validate_Employee_input_EmployeeNumber(inp):
        errors = []
        # Put validation logic here
        if len(inp) > 10:
            errors.append("Employee number can't be more than 10 characters!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response

    @staticmethod
    def validate_Employee_input_Password(inp):
        errors = []
        # Put validation logic here
        if inp(len) < 6:
            errors.append("Password must be at least 6 letters long!")

        # End of validation logic
        response = {}
        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
