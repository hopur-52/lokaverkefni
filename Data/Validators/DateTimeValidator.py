import re

"""
    Work in progress, not everything implemented yet.
"""
class DateTimeValidator:
    @staticmethod
    def validate_date_time_input(inp):
        # Datetime format:  '%Y-%m-%d %H:%M:%S'
        dt_format = "%Y-%m-%d %H:%M:%S"
        return re.search(dt_format, inp)

    @staticmethod
    def validate_date_time_no_seconds_input(inp):
        # Datetime format:  '%Y-%m-%d %H:%M:%S'
        dt_format = "%Y-%m-%d %H:%M"
        return re.search(dt_format, inp)

    @staticmethod
    def validate_time_input_field(inp):
        # Time format: 'hh:mm'
        errors = []
        # 1. Check if string length is 5
        if inp.__len__() != 5:
            e_msg = "Input is too long, should be 5 but is {}".format(inp.__len__())
            errors.append(e_msg)
        else:
            # 2. Check if first 2 letters are integers
            # 3. Check if last 2 letters are integers
            print(type(inp[0]))
            if inp[0].isdigit() and inp[1].isdigit() and inp[3].isdigit() and inp[4].isdigit():
                # 4. Check if hh <= 24
                if int(inp[:-3]) > 24:
                    e_msg = "There are only 24 hours in a day"
                    errors.append(e_msg)
                # 5. Check if mm <= 60
                if int(inp[-2:]) > 60:
                    e_msg = "There are only 60 minutes in an hour"
                    errors.append(e_msg)
            else:
                e_msg = "First 2 and last 2 letters should be numbers"
                errors.append(e_msg)

        response = {}

        if errors.__len__() > 0:
            response["status"] = "error"
            response["errors"] = errors
        else:
            response["status"] = "success"

        return response
