from datetime import datetime

class Utility:
    @staticmethod
    def is_type_of_array(inp):
        if type(inp) == type([]):
            return True
        else:
            return False

    @staticmethod
    def is_type_of_datetime(inp):
        if type(inp) == type(datetime.now()):
            return True
        else:
            return False

    @staticmethod
    def is_type_of_string(inp):
        if type(inp) == type(str()):
            return True
        else:
            return False
