import string, random
"""
    This class is responsible for data security
    Currently it does nothing, its just a stub.
"""

class Crypt:

    @staticmethod
    def encrypt(data):
        return data

    @staticmethod
    def decrypt(data):
        return data

    @staticmethod
    def hashPassword(password):
        return password

    @staticmethod
    def verifyHash(hash):
        return hash

    @staticmethod
    def generate_id(size=10):
        chars = string.ascii_uppercase + string.digits
        return ''.join(random.choice(chars) for _ in range(size))



