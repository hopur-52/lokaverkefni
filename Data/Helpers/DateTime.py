from datetime import datetime
from Data.Helpers.Utility import Utility as u
from Data.Validators.DateTimeValidator import DateTimeValidator as dtv

class DateTimeHelper():

    #Gives you a datetime object for now
    @staticmethod
    def now():
        return datetime.now().replace(microsecond=0)

    @staticmethod
    def today():    # Work in progress!
        return "Should return today"

    #Breytir string i datetime object.
    @staticmethod
    def stringToDateTime(dt_str):
        if u.is_type_of_string(dt_str):
            try:
                return datetime.strptime(dt_str,'%Y-%m-%d %H:%M:%S')
            except ValueError:
                return dt_str
        else:
            return False
