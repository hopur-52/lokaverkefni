class RepositoryHelper:
    @staticmethod
    def repositoryToString(data=None):
        if data == None:
            raise AttributeError('No data')
        s = ""

        for c in data:
            s = s + str(c) + '\n'

        # Remove leading new line for the last entry
        s = s.rstrip()

        return s

    @staticmethod
    def dictionaryToString(data=None):
        if data == None:
            raise AttributeError('No data')
        s = ""
        for kt, c in data.items():
                s = s + str(c) + '\n'

        # Remove leading new line for the last entry
        s = s.rstrip()
        return s
