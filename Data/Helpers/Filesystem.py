class Filesystem:
    # This class reads file into string and
    # writes string to file
    @staticmethod
    def saveToDisk(file_url=None, data=None):
        with open(file_url, 'w') as file:
            return file.write(data)

    @staticmethod
    def readFromDisk(file_url=None):
        if file_url == None:
            raise FileNotFoundError('No file url defined, should be the first parameter.')
        with open(file_url, encoding = "UTF-8") as file:
            data = file.read()
            return data


