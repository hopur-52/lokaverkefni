from Data.Models.CarModel import CarModel
from Services.CarService import CarService
class CarRepository:
    def __init__(self):
        pass
    @staticmethod
    def getAllCars(data=None):
        if data == None:
            raise AttributeError('No data')
        cars = {}
        data = data.rstrip()
        cars_data = data.split('\n')
        for c in cars_data:
            c_arr = c.split('|')
            cars[c_arr[0]] = CarModel(c_arr[0], c_arr[1], c_arr[2], c_arr[3], c_arr[4], c_arr[5], c_arr[6])
        return cars

    def insertCar(self):
        carservice = CarService()
        carmodel = carservice.GetCarModel()
        carsinfo = [carmodel.plate, carmodel.price_cat, carmodel.price, carmodel.type, carmodel.brand,
                    carmodel.color, carmodel.fuel, carmodel.transmission]
        return carsinfo

