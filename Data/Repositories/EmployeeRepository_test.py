import unittest
from Data.Repositories.EmployeeRepository import EmployeeRepository
from Data.Models.EmployeeModel import EmployeeModel
from Data.Helpers.Repository import RepositoryHelper

"""
    This test depends upon working customer model
"""


class EmployeeRepositoryTest(unittest.TestCase):

    def setUp(self):
        employees = {}
        employees[1234] = EmployeeModel('Einar', '1234', 'password')
        employees[3421] = EmployeeModel('Palli', '3421', 'password')

        self.customers_string = RepositoryHelper.dictionaryToString(employees)

    def tearDown(self):
        pass

    def test_get_all_employees(self):
        # Take a string of customers and pass it through the function
        # Should return array of CustomerModel objects
        employees = EmployeeRepository.getAllEmployees(self.customers_string)
        for key, value in employees.items():
            self.assertIsInstance(value, EmployeeModel)


if __name__ == '__main__':
    unittest.main()
