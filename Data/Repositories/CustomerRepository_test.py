import unittest
from Data.Repositories.CustomerRepository import CustomerRepository
from Data.Models.CustomerModel import CustomerModel
from Data.Helpers.Repository import RepositoryHelper

"""
    This test depends upon working customer model
"""


class CustomerRepositoryTest(unittest.TestCase):

    def setUp(self):
        customers = {}
        customers[1234123412] = CustomerModel('Jonas', '1234123412', 'Snorrabraut///13///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1234123412341234///123///4-23[-^-]1234123412341234///321///5-20')
        customers[9876123412] = CustomerModel('Palli', '9876123412', 'Snorrabraut///13///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1234123412341234///123///4-23[-^-]1234123412341234///321///5-20')

        self.customers_string = RepositoryHelper.dictionaryToString(customers)

    def tearDown(self):
        pass

    def test_get_all_customers(self):
        # Take a string of customers and pass it through the function
        # Should return array of CustomerModel objects
        customers = CustomerRepository.getAllCustomers(self.customers_string)
        for key, value in customers.items():
            self.assertIsInstance(value, CustomerModel)


if __name__ == '__main__':
    unittest.main()
