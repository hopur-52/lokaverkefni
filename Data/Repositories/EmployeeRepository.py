from Data.Models.EmployeeModel import EmployeeModel
""" 
    This class converts string af models to a array of customer model objects
    This class also converts the array of model objects back to string
"""
class EmployeeRepository:
    @staticmethod
    def getAllEmployees(data=None):
        if data==None:
            raise AttributeError('No data')
        employess = {}
        employess_data = data.split('\n')
        for e in employess_data:
            e_arr = e.split('|')
            employess[e_arr[1]] =  EmployeeModel(e_arr[0], e_arr[1], e_arr[2])
        return employess
