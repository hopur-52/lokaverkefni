import unittest
from Data.Repositories.CarRepository import CarRepository
from Data.Models.CarModel import CarModel
from Data.Helpers.Repository import RepositoryHelper

"""
    This test depends upon working Car model
"""


class CustomerRepositoryTest(unittest.TestCase):

    def setUp(self):
        cars = {}
        cars['FD-234'] = CarModel('FD-234', 'Verðflokkur 2', '55000', 'Fólksbíll', 'Honda', 'Grár', 'Bensín')
        cars['XY-567'] = CarModel('XY-567', 'Verðflokkur 3', '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Dísel')

        self.car_string = RepositoryHelper.dictionaryToString(cars)

    def tearDown(self):
        pass

    def test_get_all_cars(self):
        # Take a string of customers and pass it through the function
        # Should return array of CustomerModel objects
        cars = CarRepository.getAllCars(self.car_string)
        for key, value in cars.items():
            self.assertIsInstance(value, CarModel)


if __name__ == '__main__':
    unittest.main()
