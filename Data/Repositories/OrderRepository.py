from Data.Models.OrderModel import OrderModel
""" 
    This class converts string af models to a array of customer model objects
    This class also converts the array of model objects back to string
"""
class OrderRepository:
    @staticmethod
    def getAllOrders(data=None):
        if data == None:
            raise AttributeError('No data')
        orders = {}
        data = data.rstrip()
        orders_data = data.split('\n')
        for o in orders_data:
            o_arr = o.split('|')
            orders[o_arr[0]] = OrderModel(o_arr[0],o_arr[1],o_arr[2],o_arr[3],o_arr[4],o_arr[5],o_arr[6],o_arr[7])
        return orders
