from Data.Models.CustomerModel import CustomerModel
""" 
    This class converts string of models to a array of customer model objects
    This class also converts the array of model objects back to string
"""
class CustomerRepository:
    @staticmethod
    def getAllCustomers(data=None):
        if data==None:
            raise AttributeError('No data')
        customers = {}
        data = data.rstrip()
        customers_data = data.split('\n')
        for c in customers_data:
            c_arr = c.split('|')
            customers[c_arr[1]] = CustomerModel(c_arr[0], c_arr[1], c_arr[2], c_arr[3], c_arr[4], c_arr[5])
        return customers
