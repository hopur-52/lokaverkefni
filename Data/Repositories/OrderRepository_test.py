import unittest
from Data.Repositories.OrderRepository import OrderRepository
from Data.Models.OrderModel import OrderModel
from Data.Helpers.Repository import RepositoryHelper

"""
    This test depends upon working customer model
"""


class OrderRepositoryTest(unittest.TestCase):

    def setUp(self):
        orders = {}
        orders[1234] = OrderModel('1234', '1234123412', 12,'card','2018-12-08 15:00:00///2018-12-18 15:00:00///AB123///y')
        orders[1235] = OrderModel('1235', '9876123412',13,'cash','2018-12-12 15:00:00///2018-12-29 15:00:00///RE976///y[-^-]2018-12-12 15:00:00///2018-12-29 15:00:00///EF123///y')

        self.orders_string = RepositoryHelper.dictionaryToString(orders)
        self.orders = orders

    def tearDown(self):
        pass

    def test_get_all_orders(self):
        # Take a string of customers and pass it through the function
        # Should return array of CustomerModel objects
        orders = OrderRepository.getAllOrders(self.orders_string)
        print("Testing %s orders" % (self.orders.__len__()))
        for key, value in orders.items():
            print('Start: %s\tEnd: %s\tCar: %s' % (value.get_leases()[0].get_start(),value.get_leases()[0].get_end(),value.get_leases()[0].get_car()))
            self.assertIsInstance(value, OrderModel)


if __name__ == '__main__':
    unittest.main()
