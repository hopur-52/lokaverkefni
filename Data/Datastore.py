from Data.Helpers.Filesystem import Filesystem
from Data.Repositories.CustomerRepository import CustomerRepository
from Data.Repositories.EmployeeRepository import EmployeeRepository
from Data.Repositories.CarRepository import CarRepository
from Data.Repositories.OrderRepository import OrderRepository
from Data.Helpers.Repository import RepositoryHelper
import os

class Datastore:
    projectdirectorypath = os.path.dirname(os.path.abspath(__file__))
    customersfilelocation = projectdirectorypath+'/Storage/customers.txt'
    employeesfilelocation = projectdirectorypath+'/Storage/employees.txt'
    carsfilelocation = projectdirectorypath+'/Storage/cars.txt'
    ordersfilelocation = projectdirectorypath+'/Storage/orders.txt'

    def __init__(self):
        self.customers = self.getAllCustomers()
        self.orders = self.getAllOrders()
        self.cars = self.getAllCars()
        self.employees = self.getAllEmployees()


    def getAllCustomers(self, file = customersfilelocation):
        data = Filesystem.readFromDisk(file)
        return CustomerRepository.getAllCustomers(data)

    def saveAllCustomers(self, file=customersfilelocation, data=None):
        if data==None:
            c_str = RepositoryHelper.dictionaryToString(self.customers)
        else:
            c_str = RepositoryHelper.dictionaryToString(data)
        Filesystem.saveToDisk(file, c_str)

    def getAllEmployees(self,file=employeesfilelocation):
        data = Filesystem.readFromDisk(file)
        return EmployeeRepository.getAllEmployees(data)

    def saveAllEmployees(self,file=employeesfilelocation, data=None):
        if data==None:
            e_str = RepositoryHelper.dictionaryToString(self.employees)
        else:
            e_str = RepositoryHelper.dictionaryToString(data)

        Filesystem.saveToDisk(file, e_str)

    def getAllCars(self, file=carsfilelocation):
        data = Filesystem.readFromDisk(file)
        return CarRepository.getAllCars(data)

    def saveAllCars(self, file=carsfilelocation, data=None):
        if data==None:
            cars_str = RepositoryHelper.dictionaryToString(self.cars)
        else:
            cars_str = RepositoryHelper.dictionaryToString(data)

        Filesystem.saveToDisk(file, cars_str)

    def getAllOrders(self, file=ordersfilelocation):
        data = Filesystem.readFromDisk(file)
        return OrderRepository.getAllOrders(data)

    def saveAllOrders(self, file=ordersfilelocation, data=None):
        if data==None:
            orders_str = RepositoryHelper.dictionaryToString(self.orders)
        else:
            orders_str = RepositoryHelper.dictionaryToString(data)

        Filesystem.saveToDisk(file, orders_str)

    def check_if_in_dict(self, key='' ,the_dict={}):
        return key in the_dict



