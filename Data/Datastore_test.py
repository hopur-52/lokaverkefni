import unittest
from Data.Datastore import Datastore
from Data.Models.CustomerModel import CustomerModel
from Data.Models.EmployeeModel import EmployeeModel
from Data.Models.CarModel import CarModel
from Data.Models.OrderModel import OrderModel
import os

"""
    This test depends upon working repositories and models.
    It tests the classes together rather then just a single one at a time
    
    
    It's still a work in progress
                Save    Get     Search
    Customer    [x]     [x]        []
    Employee    []      [x]        []
    Car         []      [x]        []
    Order       []      [x]        []
"""
class DatastoreTest(unittest.TestCase):

        def setUp(self):
            projectdirectorypath = os.path.dirname(os.path.abspath(__file__))
            self.customersfilelocation = projectdirectorypath+'/Storage/test/customers_test.txt'
            self.employeesfilelocation = projectdirectorypath+'/Storage/test/employees_test.txt'
            self.carsfilelocation = projectdirectorypath+'/Storage/test/cars_test.txt'
            self.ordersfilelocation = projectdirectorypath+'/Storage/test/orders_test.txt'

        def tearDown(self):
            pass

        def test_get_all_customers(self):
            customers = Datastore.getAllCustomers(self, self.customersfilelocation)
            for kt, customer in customers.items():
                self.assertIsInstance(customer, CustomerModel)

        def test_save_all_customers(self):
            customers = {}
            customers[1234123412] = CustomerModel('Jonas', '1234123412', 'Snorrabraut///13///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1///1234123412341234///123///4-23[-^-]2///1234123412341234///321///5-20')
            customers[1234123482] = CustomerModel('Palli', '1234123412', 'Menntavegur///1///Reykjavík///Ísland', '4564565', '2018-12-08 19:59:16', '1///1234123412341234///123///4-23')

            Datastore.saveAllCustomers(self,self.customersfilelocation,customers)

        def test_get_all_employees(self):
            employees = Datastore.getAllEmployees(self,self.employeesfilelocation)
            for employee_nr, e in employees.items():
                self.assertIsInstance(e, EmployeeModel)

        def test_save_all_employees(self):
            employees = {}
            employees[4199] = EmployeeModel("Einar Bragi",4199,"password")
            employees[4200] = EmployeeModel("Palli", 4200, "password")

            Datastore.saveAllEmployees(self, self.employeesfilelocation, employees)

        def test_get_all_cars(self):
            cars = Datastore.getAllCars(self,self.carsfilelocation)
            for license_plate, c in cars.items():
                self.assertIsInstance(c, CarModel)

        def test_save_all_cars(self):
            cars = {}
            cars['FD-234'] = CarModel('FD-234', 59000, '55000', 'Fólksbíll', 'Honda', 'Grár', 'Bensín')
            cars['XB-305'] = CarModel('XB-305', 59000, '55000', 'Fólksbíll', 'Honda', 'Rauður', 'Bensín')
            cars['XY-567'] = CarModel('XY-567', 59000, '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Dísel')

            Datastore.saveAllCars(self,self.carsfilelocation,cars)

        def test_get_all_orders(self):
            orders = Datastore.getAllOrders(self,self.ordersfilelocation)
            for order_id, o in orders.items():
                self.assertIsInstance(o, OrderModel)

        def test_save_all_orders(self):
            orders = {}
            orders[1234] = OrderModel(1234,2805922859,1234,'card','2018-12-08 19:59:16///2018-12-18 19:59:16///XB-305///79000///y///59000',59000,70000)
            orders[1244] = OrderModel(1244,2805922859,1234,'card','2018-12-02 19:59:16///2018-12-15 19:59:16///XB-305///79000///y///59000[-^-]2018-12-02 19:59:16///2018-12-15 19:59:16///XB-305///79000///y///59000',59000,70000)

            Datastore.saveAllOrders(self,self.ordersfilelocation, orders)

if __name__ == '__main__':
    unittest.main()
