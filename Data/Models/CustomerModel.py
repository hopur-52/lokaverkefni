from Data.Helpers.DateTime import DateTimeHelper as dt
class CustomerModel:
    def __init__(self,name="",kennitala="",address='',phone=int(),reg_date=dt.now(),cards=""):
        self.kennitala = kennitala
        self.name = name
        self.phone = int(phone)
        self.reg_date = reg_date
        self.cards = {}

        adr_ar = address.split('///')
        self.address = AddressModel(adr_ar[0],adr_ar[1],adr_ar[2],adr_ar[3])

        cards_arr = cards.split('[-^-]')
        for c in cards_arr:
            c_arr = str(c).split('///')
            self.cards[c_arr[0]] = CreditCardModel(c_arr[0],c_arr[1],c_arr[2])

    def __str__(self):
        cards_str = ""
        for card, card_info in self.cards.items():
            cards_str += str(card_info) + '[-^-]'
        # Remove extra symbol in last iteration [-^-] (5 characters)
        cards_str = cards_str[:-5]
        return "{}|{}|{}|{}|{}|{}".format(self.name,self.kennitala,self.address,self.phone,self.reg_date,cards_str)


class AddressModel:
    def __init__(self, street='',house ='', city ='',country=''):
        self.street = street.capitalize()
        self.house = house
        self.city = city.capitalize()
        self.country = country.upper()
    
    def __str__(self):
        return "{}///{}///{}///{}".format(self.street,self.house,self.city,self.country)

class CreditCardModel:
    def __init__(self, card_number="", card_ccv="", month_year=""):
        self.cardnumber = card_number
        self.ccv = card_ccv
        self.monthyear = month_year

    def __str__(self):
        return "{}///{}///{}".format(self.cardnumber,self.ccv,self.monthyear)
