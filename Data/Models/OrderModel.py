from datetime import datetime

class OrderModel:
    def __init__(self,o=int(1),c="",e=int(1),b="",l='',pt=int(0),p_vat=int(0), le=datetime.now().replace(microsecond=0)):
        self.order_id = o
        self.customer_id = c  # Kennitala
        self.employee_id = e  # Employee Number
        self.billing_type = b # Billing type
        self.leases = {}
        self.last_edited_at = le
        self.price_total = pt
        self.price_w_VAT = p_vat
        if type(l) == type({}):
            for key,lease in l.items():
                self.leases[key] = lease
        else:
            leases_arr = l.split('[-^-]')

            for c in leases_arr:
                c_arr = str(c).split('///')
                self.leases[c_arr[0]] = Lease(c_arr[0],c_arr[1],c_arr[2],c_arr[3],c_arr[4],c_arr[5],c_arr[6])

    def __str__(self):
        leases_string = ''
        for key,value in self.leases.items():
            leases_string += value.__str__() + "[-^-]"
        
        leases_string = leases_string[:-5]
        return "{}|{}|{}|{}|{}|{}|{}|{}".format(self.order_id, self.customer_id, self.employee_id, self.billing_type,leases_string,self.price_total,self.last_edited_at,self.price_w_VAT)


class Lease:
    def __init__(self,lid=int(),s=datetime.now().replace(microsecond=0),e=datetime.now().replace(microsecond=0),c="",p= int(0),ei='',pt=int(0)):
        self.__lease_number = lid #Leases NR.# in order.
        self.__lease_start = s
        self.lease_end   = e
        self.car = c #License plate number
        self.__price = p #price of renting car for the period without VAT or any extra costs
        self.__extra_insurance = ei.lower() #Extra insurance, y/n.
        self.price_total = pt #price with additional insurance, not including VAT.


    def __str__(self):
        return '{}///{}///{}///{}///{}///{}///{}'.format(self.__lease_number,self.__lease_start,self.lease_end,self.car,self.__price,self.__extra_insurance,self.price_total)

