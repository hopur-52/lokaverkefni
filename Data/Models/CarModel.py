class CarModel:
    def __init__(self,plate="",price=int(0),eip =int(0), car_type="", brand="", color="", fuel=""):
        self.color = color
        self.plate = plate
        self.price = price
        self.extra_ins_price = eip
        self.price_w_ins = int(price) + int(eip)
        self.brand = brand
        self.type = car_type
        self.fuel = fuel
        self.vsk = '0.11'
    

    def __str__(self):
        return "{}|{}|{}|{}|{}|{}|{}".format(self.plate,self.price,self.extra_ins_price,self.type, self.brand, self.color, self.fuel)