from Data.Helpers.Crypt import Crypt

class EmployeeModel:
    def __init__(self, name="", employee_number=int(), password=""):
        self.name = str(name)
        self.employee_number = int(employee_number)
        self.password = Crypt.hashPassword(password)

    def __str__(self):
        return "{}|{}|{}".format(self.name, self.employee_number, self.password)