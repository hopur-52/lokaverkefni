Car Rental Application
============

Háskólinn í Reykjavík

Verklegt Námskeið *1*, unnið af:

  * Arnar Freyr Kristinsson
  * Arnar Ólafsson
  * Einar Bragi Guðmundsson
  * Páll Þorsteinsson
  * Róbert Örn Karlsson




Validators
----------





***

Here's a definition list:


## Customer

        [  ]   Kennitala Validator
        [  ]   Name Validator
        [x]   Address Validator
        [  ]   Phone Validator
        [  ]   Credit Card Validator

`Please check the boxes after finishing a validator`

    Done    Validator Class                          Validation Rule
    -----------------------------------------------------------------
     [  ]       Kennitala Validator             Input must be 10 characters long

     [  ]       Kennitala Validator             Input must only contain digits



     [  ]       Phone Validator                 Must only be digits

     [  ]       Phone Validator                 Must be at least 3 digits

     [  ]       Credit Card Validator           Maximum 19 digits

     [ X ]       Address Validator               Must contain a house number

     [  X]       Address Validator               Town must be at least

     [  X]       Address Validator               Must contain a country

  --------                          -----------------------                     ----

## Car

        [  X]   License Plate Validator
        [  ]   Type Validator
        [ X ]   Brand Validator
        [  ]   Fuel Validator
        [  X]   Color Validator

`Please check the boxes after finishing a validator`

    Done    Validator Class                          Validation Rule
    -----------------------------------------------------------------
     [ X ]       License Plate Validator             Input must be shorter than 15 characters

     [  ]       Type Validator                      Input must be shorter than  characters



  --------                          -----------------------                     ----

## Order

        [  ]   Order Id Validator
        [  ]   Leases Validator
        [  ]   Billing Type Validator


`Please check the boxes after finishing a validator`

    Done    Validator Class                          Validation Rule
    -----------------------------------------------------------------
     [  ]       Order Id Validator             Always 10 characters

     [  ]       Order Id Validator             Always 10 digits

     [  ]       Billing Type validator         Only Cash or card as a string

  --------                          -----------------------                     ----

## Employee

        [  ]   Employee Id Validator
        [  ]   Password Validator


`Please check the boxes after finishing a validator`

    Done    Validator Class                          Validation Rule
    -----------------------------------------------------------------
     [  ]       Employee Id Validator             this is validation rule

     [  ]       Employee Id Validator             this is validation rule


  --------                          -----------------------                     ----

## General

        [  ]   DateTime Validator
        [  ]   Price Validator
        [  ]   Name

`Please check the boxes after finishing a validator`

    Done    Validator Class                          Validation Rule
    -----------------------------------------------------------------
     [  ]       DateTime Validator            this is validation rule

     [  ]       DateTime Validator             this is validation rule
     [  ]       Name Validator                  Can't contain special characters

  --------                          -----------------------                     ----
