from View.Helpers.MenuHelpers.CarMenu.SingleCarInfoMenuHelper import SingleCarMenuHelper
from View.Helpers.GoBackHelper import GoBack
from View.MenuViews.CustomerViews.RentCarToCustomerView import RentCarToCustomerView
from View.MenuViews.OrderViews.CarOrderTable import CarOrderTable as OrderTable
from Data.Datastore import Datastore

class CarSingleMenuView:

    def Menu(self, car):
        car_menu = SingleCarMenuHelper()
        car_menu.Menu(car)


        title = "Selected car: '{}' {} {} {}".format(car.plate, car.brand, car.type, car.fuel)
        title += '\n\t'+'Car Plate: '+str(car.plate)+'\n\t'+"Car Brand: "+str(car.brand)+\
                 "\n\tCar Type: "+str(car.type)+\
                 "\n\tExtra costs:\n\tVAT: "+str(car.vsk)+"\n\tInsurance: "+str(car.extra_ins_price)

        selection = car_menu.Menu(title)

        selections = car_menu.GetSelections()

        data = Datastore()

        rent_to_customer = RentCarToCustomerView(car.plate)
        rental_history = OrderTable(car.plate)

        menus = [rent_to_customer, rental_history]

        for i in range(0, len(selections)-1):
            if selections[i] == selection:
                menus[i].Menu()

        go_back = GoBack()
        go_back.GoBackToMainMenu(selection)

