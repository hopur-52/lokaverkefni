from Data.Datastore import Datastore
from View.Helpers.TableHelper import Table
from Services.CarService import CarService
from datetime import datetime
from View.MenuViews.CarViews.CarSingleMenuView import CarSingleMenuView

class AvailableCarsView:
    def Menu(self):
        data = Datastore()

        column_names = ['License Plate','Price','Type','Brand','Color','Fuel']
        column_data = {}

        available_cars = CarService.get_available_cars(datetime.now(),datetime.now(), data.cars, data.orders)

        for key, car in data.cars.items():
            if key in available_cars:
                column_data[key] = [car.price,car.type,car.brand,car.color,car.fuel]

        c = Table(column_names,column_data, 'Available Cars')

        car = c.print_table()
        single_view = CarSingleMenuView()

        license_plate = car.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.

        single_view.Menu(data.cars[license_plate])
