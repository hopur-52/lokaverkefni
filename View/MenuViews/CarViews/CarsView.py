from View.MenuViews.CarViews.SearchACar import SearchCarView
from View.Helpers.MenuHelpers.CarMenu.CarMenuHelper import CarsMenu
from View.MenuViews.CarViews.AvailableCarsView import AvailableCarsView
from View.MenuViews.CarViews.UnavailableCarsView import UnavailableCarsView
from View.Helpers.GoBackHelper import GoBack

class CarView:

    def Menu(self):
        from View.MenuViews.MainMenuView import MainMenu#Importing above creates a conflict
        car_menu = CarsMenu()
        selection = car_menu.Menu()
        selections = car_menu.GetSelections()

        search_a_car_menu = SearchCarView()
        available_cars = AvailableCarsView()
        rented_cars = UnavailableCarsView()

        menus = [search_a_car_menu, available_cars, rented_cars]

        for i in range(0, len(selections)-1):
            if selections[i] == selection:
                menus[i].Menu()

        go_back = GoBack()
        go_back.GoBackToMainMenu(selection)




