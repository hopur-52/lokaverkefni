from Data.Datastore import Datastore
from View.Helpers.TableHelper import Table
from Services.CarService import CarService
from datetime import datetime

class SelectCarView:
    def __init__(self,cars=None,orders=None):
        if cars != None:
            self.cars = cars
        else:
            data = Datastore()
            self.cars = data.cars

        if orders != None:
            self.orders = orders
        else:
            data = Datastore()
            self.orders = data.orders
    def Menu(self):

        # Test input 1
        column_names = ['License Plate','Price','Type','Brand','Color','Fuel']
        column_data = {}

        for key, car in self.cars.items():
            column_data[key] = [car.price,car.type,car.brand,car.color,car.fuel]

        c = Table(column_names,column_data,'Available Cars')

        car = c.print_table()
        return car

