from Services.CarService import CarService
from Data.Datastore import Datastore
from View.Helpers.ClearTheScreenHelper import ClearScreen
class ReturnCarView:

    def Menu(self):
        cls = ClearScreen()
        cls.ClearScreen(self)
        print("Return Car")
        carnumber = input("Enter Licence Plate: ")
        data = Datastore()
        data.orders = CarService().confirm_car_return(carnumber,data.orders)
        data.saveAllOrders()

        from View.Helpers.GoBackHelper import GoBack as GoBack
        GoBack.GoBackToMainMenu('Back to Main Menu')



