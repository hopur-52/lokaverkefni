from View.Helpers.TableHelper import Table
from View.Inputs.Car.CarSearchInput_test import CarSearchInput
from Data.Datastore import Datastore
from View.MenuViews.CarViews.CarSingleMenuView import CarSingleMenuView


class SearchCarView:
    def __init__(self,title="Search Results"):
        self.title = title

    def Menu(self):
        data = Datastore()

        search_results = CarSearchInput().input_field(data.cars)

        column_names = ['License Plate','Price','Type','Brand','Color','Fuel']
        column_data = {}
        for key, car in search_results.items():
            column_data[key] = [car.price,car.type,car.brand,car.color,car.fuel]

        c = Table(column_names,column_data, self.title)

        car = c.print_table()
        single_view = CarSingleMenuView()

        license_plate = car.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.

        single_view.Menu(data.cars[license_plate])


