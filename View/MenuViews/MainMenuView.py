from View.Helpers.FormHelper import Form
from View.Helpers.MenuHelpers.MainMenuMenuHelper import MainMenuMenuHelper
from View.MenuViews.CarViews.CarsView import CarView
from View.MenuViews.CustomerViews.CustomersView import CustomersView
from View.MenuViews.CustomerViews.RegisterACustomerView import RegisterCustomerView
from View.MenuViews.CustomerViews.LookUpACustomerView import LookUpCustomerView
from View.MenuViews.CarViews.ReturnCarView import ReturnCarView
from View.MenuViews.PriceListView.PriceCategoryView import PriceListCategoryView

class MainMenu:
    @staticmethod
    def Menu():
        mainmenu = MainMenuMenuHelper()#Prints Main Menu
        selection = mainmenu.Menu()
        selections = mainmenu.GetSelections()


        car_menu = CarView()
        customer_menu = CustomersView()
        register_customer_menu = RegisterCustomerView()
        look_up_customer_menu = LookUpCustomerView()
        return_car_menu = ReturnCarView()
        price_cat_menu = PriceListCategoryView()

        menus = [car_menu, customer_menu, register_customer_menu, look_up_customer_menu, return_car_menu,
                 price_cat_menu]

        for i in range(0, len(selections)):
            if selections[i] == selection:
                menus[i].Menu()


