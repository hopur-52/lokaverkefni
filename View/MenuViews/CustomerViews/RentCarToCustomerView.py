from Data.Datastore import Datastore
from Services.OrderService import OrderService
class RentCarToCustomerView:
    def __init__(self, plate=None):
        self.plate = plate

    def Menu(self):
        print("Rent this car to a customer ")
        data = Datastore()
        orders = OrderService.create_order(data.cars,data.orders,self.plate)
        data.orders = orders
        data.saveAllOrders()
