from View.Helpers.MenuHelpers.CustomerMenu.CreateCustomerMenuHelper import CreateCustomerMenu
from View.Helpers.GoBackHelper import GoBack
from View.MenuViews.CustomerViews.CustomerDetailsView import CustomerDetailsView

class RegisterCustomerView:

    def Menu(self):
        from View.MenuViews.MainMenuView import MainMenu#Importing above creates a conflict
        register_customer_menu = CreateCustomerMenu()
        selection = register_customer_menu.Menu()
        selections = register_customer_menu.GetSelections()

        edit_customer_details = CustomerDetailsView()

        menus = [edit_customer_details]

        for i in range(0, len(selections)-1):
            if selections[i] == selection:
                menus[i].Menu()

        if selection == 'Back To Main Menu':
            from View.MenuViews.MainMenuView import MainMenu  # Placed above creates a conflict
            main_menu = MainMenu()
            main_menu.Menu()

        go_back = GoBack()
        go_back.GoBackToCustomers(selection)


