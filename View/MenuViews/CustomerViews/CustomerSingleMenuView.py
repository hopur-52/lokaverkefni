from View.Helpers.MenuHelpers.CustomerMenu.SingleCustomerMenuHelper import SingleCustomerMenuHelper
from View.MenuViews.OrderViews.CustomerOrdersTable import CustomerOrderTable
from View.MenuViews.CustomerViews.CustomerDetailsView import CustomerDetailsView
from View.Helpers.GoBackHelper import GoBack

class CustomerSingleMenuView:
    def Menu(self,customer):
        from View.MenuViews.MainMenuView import MainMenu#Importing above creates a conflict
        car_menu = SingleCustomerMenuHelper([],customer.name)
        selection = car_menu.Menu("Selected customer: '{}' {} {}".format(customer.name, customer.kennitala, customer.phone))
        selections = car_menu.GetSelections()


        rental_history = CustomerOrderTable(customer.kennitala)
        customer_edit = CustomerDetailsView(customer)

        menus = [customer_edit,rental_history]

        for i in range(0, len(selections)-1):
            if selections[i] == selection:
                menus[i].Menu()

        go_back = GoBack()
        go_back.GoBackToMainMenu(selection)
