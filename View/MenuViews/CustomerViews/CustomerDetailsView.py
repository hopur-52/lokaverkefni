from Services.CustomerService import CustomerService
from Data.Datastore import Datastore
from View.Helpers.ClearTheScreenHelper import ClearScreen as cls
from View.Helpers.GoBackHelper import GoBack

class CustomerDetailsView:
    def __init__(self,customer=None):
        if customer != None:
            self.customer = customer

    def Menu(self,msg=""):

        if msg != "":
            cls.ClearScreen(self)
            print("\033[0;31;40m"+msg+"")
        data = Datastore()
        customer_dict = data.customers

        updated_dict = CustomerService.update_customer_info(self.customer.kennitala, customer_dict, data.orders)
        data.customers = updated_dict
        data.saveAllCustomers()
        mess = "Customer '{}' has been updated".format(self.customer.name)
        print(mess)
        go_back = GoBack()
        go_back.GoBackToMainMenu('Back To Main Menu')
