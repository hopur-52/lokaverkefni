from View.MenuViews.CarViews.SearchACar import SearchCarView
from View.Helpers.MenuHelpers.CarMenu.CarMenuHelper import CarsMenu
from View.Helpers.MenuHelpers.CarMenu.AvailableCarsMenuHelper import AvailableCarsMenuHelper
from View.Helpers.MenuHelpers.CarMenu.RentedCarsMenuHelper import RentedCarsMenuHelper
from View.Helpers.MenuHelpers.CustomerMenu.CustomersMenuHelper import CustomersMenuHelper
from View.MenuViews.CustomerViews.RegisterACustomerView import RegisterCustomerView
from View.MenuViews.CustomerViews.LookUpACustomerView import LookUpCustomerView
from View.MenuViews.OrderViews.LookUpOrder import LookUpOrderView
from View.Helpers.GoBackHelper import GoBack
class CustomersView:

    def Menu(self):
        customer_menu = CustomersMenuHelper()
        selection = customer_menu.Menu()
        selections = customer_menu.GetSelections()

        register_a_new_customer = RegisterCustomerView()
        look_up_a_customer = LookUpCustomerView()
        look_up_order = LookUpOrderView()


        menus = [register_a_new_customer, look_up_a_customer, look_up_order]

        for i in range(0, len(selections)-1):
            if selections[i] == selection:
                menus[i].Menu()

        go_back = GoBack()
        go_back.GoBackToMainMenu(selection)


