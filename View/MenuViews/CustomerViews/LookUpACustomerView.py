from View.Inputs.Customer.CustomerSearchInput import CustomerSearchInput as csi
from Data.Datastore import Datastore
from View.Helpers.TableHelper import Table
from View.MenuViews.CustomerViews.CustomerSingleMenuView import CustomerSingleMenuView

class LookUpCustomerView:

    def Menu(self):
        data = Datastore()
        search_results = csi.input_field(data.customers)

        column_names = ['Kennitala', 'Name','Phone','Address','City']
        column_data = {}

        single_view = CustomerSingleMenuView()
        if list(search_results.keys()).__len__() == 1:
            key = list(search_results.keys())[0]
            single_view.Menu(data.customers[key])
        else:
            for key, customer in search_results.items():
                column_data[key] = [customer.name, customer.phone, customer.address.street + ' ' + customer.address.house, customer.address.city]

            c = Table(column_names,column_data,'Search Results')

            car = c.print_table()

            kennitala = car.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.

            single_view.Menu(data.customers[kennitala])

