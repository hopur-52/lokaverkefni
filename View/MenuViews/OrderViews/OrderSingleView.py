from Services.OrderService import OrderService
from Data.Datastore import Datastore

class OrderSingleView:
    def Menu(self,order):
        data = Datastore()
        updated_dict = OrderService.edit_order(order.order_id, data.orders, data.cars)
        data.orders = updated_dict
        data.saveAllOrders()

