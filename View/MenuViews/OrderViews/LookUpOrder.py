from View.Inputs.Order.SearchOrderInput import OrderSearchInput
from Data.Datastore import Datastore
from View.MenuViews.OrderViews.OrderSingleView import OrderSingleView
from View.Helpers.TableHelper import Table

class LookUpOrderView:

    def Menu(self):
        data = Datastore()
        search_results = OrderSearchInput.input_field(data.orders)

        column_names = ['Order id', 'Kennitala','Employee Name','Last updated', 'Price with VAT']
        column_data = {}


        single_view = OrderSingleView()
        if list(search_results.keys()).__len__() == 1:
            key = list(search_results.keys())[0]
            single_view.Menu(data.orders[key])
        else:
            for key, order in search_results.items():
                column_data[key] = [order.customer_id, order.employee_id, order.last_edited_at, order.price_w_VAT]

            c = Table(column_names,column_data,'Search Results')

            car = c.print_table()

            order_id = car.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.

            single_view.Menu(data.orders[order_id])





