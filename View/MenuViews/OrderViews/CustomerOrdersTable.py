from View.Helpers.TableHelper import Table
from Data.Datastore import Datastore
from View.MenuViews.OrderViews.OrderSingleView import OrderSingleView

class CustomerOrderTable:

    def __init__(self, kennitala=""):
        data = Datastore()

        if kennitala != "":
            self.orders = {}
            for order_id, order in data.orders.items():
                if kennitala == order.customer_id:
                    self.orders[order_id] = order
        else:
            self.orders = data.orders


    def Menu(self):


        search_results = self.orders

        column_names = ['Order id','Price','VAT','Last updated']
        column_data = {}
        for key, order in search_results.items():
            column_data[key] = [order.price_total,order.price_w_VAT,order.last_edited_at]

        c = Table(column_names,column_data,'Orders for Car:')

        order = c.print_table()

        # Then need to go to Order Menu
        single_view = OrderSingleView()

        order_id = order.split('|')[1].strip()   # Shitmix cause FormHelper aint doing it right.

        single_view.Menu(self.orders[order_id])

