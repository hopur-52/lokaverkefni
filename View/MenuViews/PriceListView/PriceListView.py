from View.Helpers.MenuHelpers.PriceMenu.PriceListMenuHelper import PriceListMenuHelper
from Data.Models.CarModel import CarModel
from View.Helpers.GoBackHelper import GoBack
class PriceListView:
    def Menu(self, selection):
        car_model = CarModel()
        extra_ins_price = {'ins_price': car_model.extra_ins_price}
        price_list_menu = PriceListMenuHelper().Menu(selection, extra_ins_price)

        go_back = GoBack()
        go_back.GoBackToPriceCategory(selection)
