from View.Helpers.MenuHelpers.PriceMenu.PriceCategoryMenuHelper import PriceCategoryMenuHelper
from View.MenuViews.PriceListView.PriceListView import PriceListView
from View.Helpers.GoBackHelper import GoBack
class PriceListCategoryView:
    def Menu(self):
        price_cat_menu = PriceCategoryMenuHelper()
        selection = price_cat_menu.Menu()
        selections = price_cat_menu.GetSelections()

        go_back = GoBack()
        go_back.GoBackToAvailableCars(selection)
        go_back.GoBackToRentedCars(selection)
        go_back.GoBackToMainMenu(selection)
        price_list_view = PriceListView().Menu(selection)