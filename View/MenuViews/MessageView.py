from View.MenuViews.MainMenuView import MainMenu
class Messages:

    def Back(self):
        print('Press 0 to go back to Main Menu...')
        key = input()
        if (key == 0):
            main_menu = MainMenu()

    def CustomerHasBeenRegisteredMessage(self, customerkennitala):
        print('Customer '+customerkennitala+' has been registered!')
        self.Back()

    def CarHasBeenRentedMessage(self, customername):
        print('Car has been rented to customer '+customername+'!')
        self.Back()

    def CarHasBeenReturnedMessage(self):
        print('Car has been returned!\nThe car is now available for rent!')