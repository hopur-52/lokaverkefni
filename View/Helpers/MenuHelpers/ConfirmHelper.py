from View.MenuViews.CarViews.CarsFoundView import CarFoundView
from View.OrderViews.OrderCarView import RentToNewCustomerView
class Confirm:
    def ConfirmFindCars(self, selection, cars_found):
        if selection == 'Confirm':
            return CarFoundView().Menu(cars_found)


    def ConfirmOrder(self, selection):
        if selection == 'Confirm':
            return RentToNewCustomerView().Menu()
