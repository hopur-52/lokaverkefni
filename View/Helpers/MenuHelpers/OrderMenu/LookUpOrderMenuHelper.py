from View.Helpers.FormHelper import Form
class LookUpOrderMenu:

    def __init__(self):
        self.selections = ['Order Number', 'Kennitala', 'Name', 'Go Back']

    def GetSelections(self):
        return self.selections

    def Menu(self):
        lookup = "LOOK UP ORDER\n---------------------------------------------------------------------------"
        lookUpInput = self.selections
        lookUpLen = len(lookUpInput)
        lookupForm = Form(lookUpLen, lookup)
        print(lookupForm)
        return lookupForm.SelectionInputs(lookUpInput, lookupForm, 'q')