from View.Helpers.FormHelper import Form
class OrderCarMenuHelper:
    def __init__(self):
        self.selections = ['Rent date', 'Return date', 'Payment method:\n \t1. Credit card\n \t2. Debit card\n \t3. Cash\n \tOrder number', 'Confirm','Back to cars']

    def GetSelections(self):
        return self.selections

    def Menu(self):
        create = 'Order for car: "License Plate"\n{:^15}|'.format('-')
        inplen = len(self.selections)
        form = Form(inplen, create)
        form.GetInputs()
        form.SelectionInputs(self.GetSelections(), form, 'q')