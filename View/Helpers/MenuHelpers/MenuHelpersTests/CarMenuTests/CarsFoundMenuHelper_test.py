from View.Helpers.MenuHelpers.CarMenu.CarsFoundMenuHelper import CarsFoundMenuHelper
class MenuHelperCarsFoundMenuTest:
    def CarsFoundMenu(self):
        carsfoundmenu = CarsFoundMenuHelper()
        car_columnnames1 = ['plate', 'price category', 'price', 'car type', 'brand', 'color', 'fuel']

        car_colum_items1 = {'AB123': [13500, 15000, '4x4', 'land rover', 'white', 'oct95'],
                            'RE652': [14000, 14353, 'normal', 'yaris', 'orange', 'oct95']}

        carsfoundmenu.Menu(car_columnnames1, car_colum_items1)

def main():
    carsfoundmenuHelperTest = MenuHelperCarsFoundMenuTest()
    carsfoundmenuHelperTest.CarsFoundMenu()
main()
