from View.Helpers.MenuHelpers.CarMenu.CarHistoryMenuHelper import CarHistoryMenuHelper
class MenuHelperCarHistoryMenuTest:
    def CarHistoryMenu(self):
        carhistorymenu = CarHistoryMenuHelper()
        car_columnnames1 = ['plate', 'Customer name', 'Start date', 'End date']

        car_colum_items1 = {'AB123': ['Arnar Freyr', '3.12.18', '3.01.19'],
                            'BA321': ['Pete Sahut', '4.01.19', '18.01.19']}

        carhistorymenu.Menu(car_columnnames1, car_colum_items1)

def main():
    carhistorymenuHelperTest = MenuHelperCarHistoryMenuTest()
    carhistorymenuHelperTest.CarHistoryMenu()
main()
