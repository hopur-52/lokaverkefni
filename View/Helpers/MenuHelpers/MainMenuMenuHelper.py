from View.Helpers.FormHelper import Form
from View.Helpers.ClearTheScreenHelper import ClearScreen

class MainMenuMenuHelper:
    def __init__(self, items = [], title = 'Menu'):
        self.__items = enumerate(items, 1)
        self.__title = title
        self.selections = ['Cars', 'Customers', 'Register a new customer',
                           'Look Up A Customer', 'Return Car', 'PriceList']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        desc = "\033[1;31;40m press 'q' to quit\tpress 'enter' to navigate main menu\n\n"
        title = 'Car Rental Software\n\t' + desc + '\033[1;36;40m Main menu'.format('-')
        selectionslen = len(self.selections)
        mainMenuForm = Form(selectionslen, title)
        selected = mainMenuForm.Selections(self.selections, 'q')
        return selected


