from View.Helpers.FormHelper import Form
class LookUpCustomerMenu:

    def __init__(self):
        self.selections = ['SSN', 'Name', 'Address', 'Go Back']

    def GetSelections(self):
        return self.selections

    def Menu(self):
        lookup = "LOOK UP CUSTOMER\n---------------------------------------------------------------------------"
        lookUpInput =  self.selections
        lookUpLen = len(lookUpInput)
        lookupForm = Form(lookUpLen, lookup)
        print(lookupForm)
        return lookupForm.SelectionInputs(lookUpInput, lookupForm, 'q')