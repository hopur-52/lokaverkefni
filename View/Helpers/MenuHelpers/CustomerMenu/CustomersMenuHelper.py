from View.Helpers.FormHelper import Form

class CustomersMenuHelper:
    def __init__(self):
        self.selections = ['Register a New Customer', 'Look Up A Customer', 'Look Up A Order', 'Back To Main Menu']

    def GetSelections(self):
        return self.selections

    def Menu(self):

        everythingelse = "Customers\n---------------------------------------------------------------------------"
        inputs = self.selections
        inputslen = len(inputs)
        form = Form(inputslen, everythingelse)
        print(form.everythingelse)
        return form.SelectionInputs(inputs, form, 'q')