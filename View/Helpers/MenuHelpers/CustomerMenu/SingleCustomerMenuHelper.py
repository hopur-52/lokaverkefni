from View.Helpers.FormHelper import Form

class SingleCustomerMenuHelper:
    def __init__(self, items = [], title = 'Menu'):
        self.__items = enumerate(items, 1)
        self.title = title
        self.selections = ['Edit customer details','Rental history','Back To Main Menu']

    def GetSelections(self):
        return self.selections

    def Menu(self,title="Single Customer Info"):
        desc = "\033[1;31;40m press 'q' to quit\tpress 'enter' to navigate main menu\n\n"
        title = 'Car Rental Software\n\t' + desc + '\033[1;36;40m Customer: {}'.format(self.title)
        selectionslen = len(self.selections)
        mainMenuForm = Form(selectionslen, title)
        selected = mainMenuForm.Selections(self.selections, 'q')
        return selected
