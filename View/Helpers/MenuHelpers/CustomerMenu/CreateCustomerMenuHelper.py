from View.Helpers.FormHelper import Form
from Services.CustomerService import CustomerService as cs
from Data.Datastore import Datastore
import os

class CreateCustomerMenu:
    def __init__(self, items = [], title = 'Menu'):
        self.__items = enumerate(items, 1)
        self.__title = title
        self.selections = ['Edit customer details', 'Back to Main Menu']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        data = Datastore()
        create = "CREATE A CUSTOMER\n---------------------------------------------------------------------------"
        customer = False
        while customer == False:
            customer = cs.register_customer(data.customers)

        data.customers[customer.kennitala] = customer

        data.saveAllCustomers()
