from View.Helpers.FormHelper import Form

class SingleCarMenuHelper:
    def __init__(self, items = [], title = 'Menu'):
        self.__items = enumerate(items, 1)
        self.title = title
        self.selections = ['Rent car to Customer','See Rental History','Back To Main Menu']

    def GetSelections(self):
        return self.selections

    def Menu(self,carmodel, title="Single Car Info"):
        desc = "\033[1;31;40m press 'q' to quit\tpress 'enter' to navigate main menu\n\n"
        title = 'Car Rental Software\n\t' + desc + '\033[1;36;40m Main menu'.format('-')

        selectionslen = len(self.selections)
        mainMenuForm = Form(selectionslen, title)
        selected = mainMenuForm.Selections(self.selections, 'q')
        return selected
