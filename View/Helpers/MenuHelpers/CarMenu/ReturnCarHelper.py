from View.Helpers.FormHelper import Form
class ReturnCarHelper:

    def __init__(self):
        self.selections = ['Enter Order Number', 'Enter License plate', 'Back To Main Menu']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        create = "RETURN CAR\n---------------------------------------------------------------------------"
        inp = self.selections
        inplen = len(inp)
        form = Form(inplen, create)
        print(form)
        return form.SelectionInputs(inp, form, 'q')