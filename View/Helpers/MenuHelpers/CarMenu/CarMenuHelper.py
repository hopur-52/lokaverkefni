from View.Helpers.FormHelper import Form

class CarsMenu:
    def __init__(self):
        self.selections = ['Search a car', 'Available Cars', 'Rented cars', 'Back To Main Menu']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        everythingelse = "CARS\n---------------------------------------------------------------------------"
        inputs = self.selections
        inputslen = len(inputs)
        form = Form(inputslen, everythingelse)
        print(form.everythingelse)
        return form.SelectionInputs(inputs, form, 'q')