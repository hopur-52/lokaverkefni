from View.Helpers.FormHelper import Form
from View.Helpers.ClearTheScreenHelper import ClearScreen
#from View.Helpers.MenuHelpers.

class SearchACarMenuHelper:

    def __init__(self):
        self.selections = ['Go Back']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        title = 'Search A Car\n{:^15}|'.format('-')
        print("Type in a car number, or car type to search for a car (Press 1 to search for the car)")

        selectionslen = len(self.selections)
        searchACarForm = Form(selectionslen, title)
        searchACarForm.GetInputs()
        return searchACarForm.Selections(self.GetSelections(), 'q')



