from View.Helpers.FormHelper import Form
class PriceCategoryMenuHelper:

    def __init__(self):
        self.selections = ['Wagon', 'SUV', 'Truck', 'Go Back To Available Cars', 'Go Back To Rented Cars', 'Back To Main Menu']
    def GetSelections(self):
        return self.selections

    def Menu(self):
        title = 'Select A Price Category Type\n{:^15}|'.format('-')
        selections = self.selections
        selectionslen = len(selections)
        mainMenuForm = Form(selectionslen, title)
        return mainMenuForm.Selections(selections, '1')

