"""The purpose of this class is to act like a form. The user can select some options and decide to update that option or just get that option
and work with it further."""
from View.Helpers.ClearTheScreenHelper import ClearScreen

class Form:
    def __init__(self, InputsLength, everythingelse):
        #Initialize inputs
        self.inputs = []
        for i in range(0, InputsLength):
            self.inputs.append("0")
        self.everythingelse = everythingelse


    def ClearTheScreen(self):
        clear = ClearScreen
        clear.ClearScreen(self)
    #returns a input (For testing purposes)
    #Adapted from: https://stackoverflow.com/questions/21046717/python-mocking-raw-input-in-unittests
    def get_input(self, text):
        return input(text)

    def GetCombinedInput(self, text):
        return input(text)

    #When input is stored in the selection, call this.
    def GetChangeInput(self, text):
        return input(text)

    #Gets the initilaziated inputs
    def GetInputs(self):
        return self.inputs

    # Get the current option inside an array split on some delimeter so each element is easilly accessable
    def GetOption(self, option):
        return option

    def printEverythingElse(self):
        return self.everythingelse

    #Prints out the options for the inputs
    def CreateInputs(self, options, selection, clear=True):
        counter = 0
        if clear == True:
            self.ClearTheScreen()

        print(self.printEverythingElse())

        #Populates the options and color selected option
        for option in options:
            if(counter == selection):
               #ANSI color: \033[1;32;40m
               #See here: http://ozzmaker.com/add-colour-to-text-in-python/
               print("\t\033[1;32;40m"+option+"\t\033[1;31;40m (press 's' to select)")
            else:
              print("\033[1;37;40m"+option)
            counter = counter + 1

    def SetDelimeter(self, delimeter=None):
        return delimeter

    #Select some option or edit it
    def Selections(self, options, finishkey="q"):
        selection = 0
        counter = 0
        keyoptions ={'up':'u','down':'', 'edit':'e', 'select':'s'}
        self.delimeter = self.SetDelimeter()
        inputs = self.GetInputs()
        Form.CreateInputs(self, options, selection)
        #Keep the original array as the options array will chang
        optionsoriginal = []
        for option in options:
            optionsoriginal.append(option)
        while True:
            key = self.get_input('')
            #down
            if (key == keyoptions['down']):
                print("\033[1;37;40m")
                counter = counter + 1
                if (counter >= len(options)):
                    selection = 0
                    counter = 0
                else:
                  selection = selection + 1
            #Up
            elif(key == keyoptions['up']):
                print("\033[1;37;40m")
                counter -= 1
                if(selection <= 0):
                    selection = (len(options)-1)
                    counter = (len(options)-1)
                else:
                  selection = selection - 1

            elif(key == keyoptions['select']):
                print("\033[1;37;40m")
                return options[selection]
            #Stop changing things...Quit loop
            elif(key == finishkey):
                return inputs
            #No else, continue until correct input is matched
            if (key == keyoptions['edit']):
                option = optionsoriginal[selection]
                self.ChangeOptionToInput(options, selection, inputs,option)
                Form.CreateInputs(self, options, selection)
            else:
             print("\033[1;37;40m")
             Form.CreateInputs(self, options, selection)

    #Change the option so that the input is shown with the option
    def ChangeOptionToInput(self, options, optionNumber, inputs, option):
         options[optionNumber] = option  # Reset option to clear the updated input if neccessary
         inp = self.GetChangeInput('')
         options[optionNumber] = str(option+': '+inp)
         inputs[optionNumber] = inp
         return inputs

    #Dependency injection - The instance of the class (the object) is inserted as a parameter
    #This function Displays the input options and returns the inputs from the user
    def SelectionInputs(self, options, form, finishkey):
        inputs = form.Selections(options, finishkey)
        form.CreateInputs(options, inputs)
        return inputs

