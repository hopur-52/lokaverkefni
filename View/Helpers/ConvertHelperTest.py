import unittest
from View.Helpers.ConvertHelper import ConvertHelper

class ConvertHelperTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_convertListTupleElementToListForTableHelper(self):
        twodarr = [('AB123', [13500, 15000, '4x4', 'land rover', 'white', 'oct95']),
                          ('RE652', [14000, 14353, 'normal', 'yaris', 'orange', 'oct95'])]
        self.assertCountEqual(ConvertHelper.convertListTupleElementToListForTableHelper(self, twodarr),
                              [['AB123', 13500, 15000, '4x4', 'land rover', 'white', 'oct95'],
                               ['RE652', 14000, 14353, 'normal', 'yaris', 'orange', 'oct95']]
                              )

    def test_convertTable2dArrayToString(self):
        twodarray = [
            ['AB123', 13500, 15000, '4x4', 'land rover', 'white', 'oct95'],
            ['RE652', 14000, 14353, 'normal', 'yaris', 'orange', 'oct95']
        ]
        self.assertEqual(ConvertHelper.convertTable2dArrayToString(self, twodarray),
                         ['AB123|13500|15000|4x4|land rover|white|oct95|',
                          'RE652|14000|14353|normal|yaris|orange|oct95|'
                          ]
                         )

    def test_convertDictonaryToTupleList(self):
        dict = {'AB123':[13500,15000,'4x4','land rover','white','oct95'],'RE652':[14000,14353,'normal','yaris','orange','oct95']}
        self.assertEqual(ConvertHelper.convertDictonaryToTupleList(self, dict),
                         [('AB123', [13500, 15000, '4x4', 'land rover', 'white', 'oct95']),
                          ('RE652', [14000, 14353, 'normal', 'yaris', 'orange', 'oct95'])]
                              )
    def test_convertDictonaryToStringListTableHelper(self):
        dict = {'AB123':[13500, 15000,'4x4','land rover','white','oct95'],'RE652':[14000,14353,'normal','yaris','orange','oct95']}
        converthelper = ConvertHelper()
        self.assertCountEqual(converthelper.convertDictonaryToStringListTableHelper(dict),
                              ['AB123|13500|15000|4x4|land rover|white|oct95|',
                               'RE652|14000|14353|normal|yaris|orange|oct95|'
                               ]
                         )

if __name__ == '__main__':
    unittest.main()
