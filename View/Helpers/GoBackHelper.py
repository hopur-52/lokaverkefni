class GoBack:
    def GoBackToMainMenu(self, selection):
        if selection == 'Back To Main Menu':
            from View.MenuViews.MainMenuView import MainMenu  # Placed above creates a conflict
            main_menu = MainMenu()
            main_menu.Menu()
    def GoBackToCars(self, selection):
        if selection == 'Go Back':
            from View.MenuViews.CarViews.CarsView import CarsMenu  # Placed above creates a conflict
            cars_menu = CarsMenu()
            cars_menu.Menu()

    def GoBackToAvailableCars(self, selection):
        if selection == 'Go Back To Available Cars':
            from View.MenuViews.CarViews.AvailableCarsView import AvailAbleCarsView  # Placed above creates a conflict
            customer_menu = AvailAbleCarsView()
            customer_menu.Menu()

    def GoBackToRentedCars(self, selection):
        if selection == 'Go Back To Rented Cars':
            from View.MenuViews.CarViews.RentedCarsView import RentedCarsView
            rented_cars_menu = RentedCarsView()
            rented_cars_menu.Menu()

    def GoBackToCustomers(self, selection):
        if selection == 'Go Back':
            from View.MenuViews.CustomerViews.CustomersView import CustomersView  # Placed above creates a conflict
            customer_menu = CustomersView()
            customer_menu.Menu()


    def GoBackToPriceCategory(self, selection):
        if selection == 'Go Back':
            from View.MenuViews.PriceListView.PriceCategoryView import PriceListCategoryView # Placed above creates a conflict
            price_cat_menu = PriceListCategoryView()
            price_cat_menu.Menu()

    def GoBackToSearchCar(self, selection):
        if selection == "Search for a new car":
            from View.MenuViews.CarViews.SearchACar import SearchCarView
            searchcarview = SearchCarView()
            searchcarview.Menu()


