"""
Converts dictonary to a string array
"""
class ConvertHelper:
    def __init__(self):
        pass

    def convertDictonaryToStringListTableHelper(self, dict):
        tupleList = self.convertDictonaryToTupleList(dict)
        twoDList = self.convertListTupleElementToListForTableHelper(tupleList)
        stringarray = self.convertTable2dArrayToString(twoDList)
        return stringarray

    #Converts a dictonarty to a tuple list
    def convertDictonaryToTupleList(self, dict):
        normalarr = []
        for dictitem in dict.items():
            normalarr.append(dictitem)
        return normalarr

    #Converts the 2D list into 1d list
    def convertListTupleElementToListForTableHelper(self, normalarray):
        twodarr = []
        for i in range(0, len(normalarray)):
            twodarr.append(normalarray[i][1])

        for i in range(0, len(normalarray)):
            twodarr[i].insert(0,normalarray[i][0])
        return twodarr

    def convertTable2dArrayToString(self, twodarray):
        stringelement = ''
        stringarray = []
        for i in range(0, len(twodarray)):
            for item in twodarray[i]:
                stringelement += str(item)+'|'
            stringarray.append(stringelement)
            stringelement = ''
        return stringarray

