"""This class clears the screen"""
import os
class ClearScreen:

    #Adapted from: https://teamtreehouse.com/community/clear-screen-for-pycharm-as-if-it-were-on-console-or-cmd
    @staticmethod
    def ClearScreen(self):
        os.system('cls' if os.name == 'nt' else 'clear')

