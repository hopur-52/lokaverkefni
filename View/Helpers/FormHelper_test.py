import unittest
from View.Helpers.FormHelper import Form
from unittest.mock import patch

class FormHelperTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    #Adapted from: https://stackoverflow.com/questions/21046717/python-mocking-raw-input-in-unittests
    @patch('View.Helpers.FormHelper.Form.get_input', return_value='1')
    def test_SelectionsEmpty(self, input):
        options = ["Select option 1", "Select option 2", "Select option 3", "Select option 4"]
        optionsLength = len(options)
        form = Form(optionsLength, '')
        self.assertSequenceEqual(form.Selections(options,'1'), ['0','0','0','0'])

    @patch('View.Helpers.FormHelper.Form.get_input', return_value='1')
    def test_SelectionsInputs(self, input):
        options = ["Select option 1", "Select option 2", "Select option 3", "Select option 4"]
        optionsLength = len(options)
        form = Form(optionsLength, '')
        self.assertSequenceEqual(form.SelectionInputs(options,form,'1'), ['0','0','0','0'])

    @patch('View.Helpers.FormHelper.Form.GetChangeInput', return_value='test')
    def test_ChangeOptionToInput(self, input):
        options = ["Select option 1", "Select option 2", "Select option 3", "Select option 4"]
        optionsLength = len(options)
        form = Form(optionsLength, '')
        self.assertSequenceEqual(form.ChangeOptionToInput(options,0,(form.GetInputs()),options[0]),['test', '0', '0', '0'])

    @patch('View.Helpers.FormHelper.Form.get_input', return_value='s')
    def test_GetOption(self,input):
        options = ["Select option 1", "Select option 2", "Select option 3", "Select option 4"]
        form = Form(3, '')
        self.assertSequenceEqual(form.Selections(options,'1'),['Select', 'option','1'])

    @patch('View.Helpers.FormHelper.Form.get_input', return_value='s')
    def test_GetDelimeter(self, input):
        form = Form(3, '')
        form.SetDelimeter(' ')
        options = ["Select option 1", "Select option 2", "Select option 3", "Select option 4"]
        self.assertSequenceEqual(form.Selections(options, '1'), ['Select', 'option', '1'])


if __name__ == '__main__':
        unittest.main()

