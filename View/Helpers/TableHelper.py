from View.Helpers.FormHelper import Form
class Table:
    def __init__(self, column_names = [], column_contents = [], title = ''):
        self.__column_names = column_names       #List of column names.
        self.__column_contents = column_contents #Dictonary where each key-pair is a row.
        self.__title = title                     #Title of table, string.

    def print_title(self):
        return '{:-^150}'.format(self.__title)

    def print_column_names(self):
        names = ''
        counter = 0
        for i in self.__column_names:
            if(counter == 1):
                names += '{:^20}'.format(i)
            else:
                names += '{:^15}'.format(i)
            counter += 1
        return names

    def print_column_items(self):
        linearr = []
        for key in self.__column_contents:
            row = ''
            row += '|{:^15}|'.format(key)
            for j in self.__column_contents[key]:
                row += '{:^15}|'.format(str(j))
            linearr.append(row)
        return linearr

    def print_table(self):
        #print the table out as one string (because of the formhelper clearing shitmix)
        tablestr = str(
                       self.print_title() +
                       '\n'+
                       self.print_column_names()
                       )
        column_contents = self.print_column_items()
        form = Form(len(column_contents), tablestr)
        return form.Selections(column_contents, '1')

