
from Data.Models.OrderModel import OrderModel
from View.Helpers.MenuHelpers.OrderMenu.OrderCarMenuHelper import OrderCarMenuHelper

class RentToNewCustomerView:
    def __init__(self):
        menu = OrderCarMenuHelper()
        self.inp = menu.Menu()

    def Menu(self):
        ordermodel = OrderModel(self.inp[0], self.inp[1], self.inp[2], self.inp[3], self.inp[4])
        return ordermodel