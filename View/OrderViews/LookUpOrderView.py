from View.Helpers.MenuHelpers.OrderMenu.LookUpOrderMenuHelper import LookUpOrderMenu
from Data.Models.CarModel import CarModel


class LookUpOrderView:
    def __init__(self):
        menu = LookUpOrderMenu()
        self.inp = menu.Menu()

    def GetLookUpOrderMenu(self):
        lookuporder = CarModel(self.inp[0], self.inp[1], self.inp[2])
        return lookuporder