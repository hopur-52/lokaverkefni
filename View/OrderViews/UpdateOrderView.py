
from Data.Models.OrderModel import OrderModel
from View.Helpers.MenuHelpers.OrderMenu.UpdateOrderHelper import UpdateOrderHelper

class UpdateOrderView:
    def __init__(self):
        menu = UpdateOrderHelper()
        self.inp = menu.Menu()

    def GetUpdateOrderMenu(self):
        ordermodel = OrderModel(self.inp[0], self.inp[1], self.inp[2], self.inp[3], self.inp[4])
        return ordermodel