from View.Helpers.MenuHelper import Menu
from Data.Models.EmployeeModel import EmployeeModel
class CreateEmployeeView:
     def __init__(self):
         menu = Menu()
         self.inputs = menu.CreateEmployeeMenu()

     def LoadAndGetCarModel(self):
          employeemodel = EmployeeModel(self.inputs[0], self.inputs[1], self.inputs[2])
          return employeeModel
