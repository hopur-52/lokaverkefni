class CarNotFoundException(Exception):
    def __init__(self,license_plate):
        print("The license plate: '%' was not found.").format(license_plate)
        super().__init__(license_plate)

