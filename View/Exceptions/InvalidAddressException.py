class InvalidEmployeeException(Exception):
    def __init__(self,address):
        print("The address '%' is invalid").format(address)
        super().__init__(address)