class InvalidInputException(Exception):
    def __init__(self,msg,errors=None):
        print(msg)
        super().__init__(msg)
        if errors == None:
            self.errors = []
        else:
            self.errors = errors

        self.print_errors()

    def print_errors(self):
        for e in self.errors:
            print('\033[1;31;35m\t' + e)
        print('\033[1;34;40m')
