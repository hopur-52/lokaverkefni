class InvalidEmployeeException(Exception):
    def __init__(self,employee_number):
        print("The employee ID '%' is wrong").format(employee_number)
        super().__init__(employee_number)