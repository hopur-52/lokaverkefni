class BillingTypeInput:
    @staticmethod
    def print_billing_options():
        print("\033[1;44;47m\tPlease choose billing method: ")
        print("\t 1. Credit Card")
        print("\t 2. Debit Card")
        print("\t 3. Cash")


    def select_billing():
        BillingTypeInput.print_billing_options()
        inp = input()
        billing_type = ''
        while str(inp).lower() not in ['1','2','3','m']:
            print('Invalid selection, please try again.\t\tType m for menu')
            inp = input()

        if inp == '1':  
            billing_type = 'Credit Card'
            return billing_type
        elif inp == '2':
            billing_type = 'Debit Card'
            return billing_type
        elif inp == '3':
            billing_type = 'Cash'
            return billing_type
        elif inp == 'm':
            return BillingTypeInput.select_billing()

