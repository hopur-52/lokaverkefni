from Data.Validators.SearchInputValidator import SearchInputValidator as siv
from View.Exceptions.InvalidInputException import InvalidInputException

class OrderSearchInput:
    @staticmethod
    def input_field(order_dict = {}):
        validated = False
        print("\n\033[1;34;40mOrder lookup\n\t(You can search for kennitala and order id (seperated by space)\n")

        while validated == False:
            input_field = input('\tSearch: ')
            check = siv.validate_search_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])

        return OrderSearchInput.search_all(input_field,order_dict)

    @staticmethod
    def search_all(search = "", order_dict = {}):
        search_params = []
        if search == "":
            return {}
        if " " not in search:
            search_params.append(search)
        else:
            search_params = search.split(" ") # Split by space
        search_results = {}

        for w in search_params:
            for key, order in order_dict.items():
                if w.lower() in str(order).lower():
                    search_results[key] = order

        return search_results
