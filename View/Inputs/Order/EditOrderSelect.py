class EditOrderInput:
    @staticmethod
    def print_edit_options():
        print('\033[1;44;47m\tPlease choose action.')
        print('1. Add Lease')
        print('2. Remove Lease')

    @staticmethod
    def choose_action():
        EditOrderInput.print_edit_options()
        inp = input()
        while str(inp).lower() not in ['1','2','m']:
            print('Invalid selection, please try again.\t\ttype m for menu')
            inp = input()

        if inp == '1':
            return '1'
        elif inp == '2':
            return '2'
        elif inp == 'm':
            return EditOrderInput.choose_action()