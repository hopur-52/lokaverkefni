from Data.Helpers.DateTime import DateTimeHelper as dt
from View.Inputs.TimeInput import TimeInput

class DateTimeInput:
    @staticmethod
    def print_options_future():
        print('\033[1;44;47m\tPlease select date and time')
        print('\t1. Today (now)')
        print('\t2. Today (manual time)')
        print('\t3. Manual date and time\n')

    @staticmethod
    def print_options_past():
        pass

    @staticmethod
    def print_options_all():
        pass

    """
        This method should only be used if you want to select a date in the future.
        If a date in the past is selected via custom date, there will be a validation error.
        
        You should rather use other methods:
            input_all
            input_past
            
    """
    @staticmethod
    def input_future():
        DateTimeInput.print_options_future()
        inp = input()
        while str(inp).lower() not in ["1","2","3","4","m"]:
            print('Invalid selection, please try again.\t\ttype m for menu')
            inp = input()

        if inp == str(1):   # Today (now)
            return dt.now()

        if inp == str(2):   # Today custom time
            return TimeInput.input_field()

        if inp == str(3):    # Custom date and time
            return input("Input date in format: 'yyyy-mm-dd hh:mm:ss'\t")

        if inp == "m":
            DateTimeInput.input_future()

    def input_field(self):
        pass

