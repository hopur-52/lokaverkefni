from Data.Validators.DateTimeValidator import DateTimeValidator as dtv
from View.Exceptions.InvalidInputException import InvalidInputException

class TimeInput:
    @staticmethod
    def input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tInput a time in format: 'hh:mm'\n")
            if dtv.validate_time_input_field(input_field)['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",dtv.validate_time_input_field(input_field)["errors"])
        return input_field

