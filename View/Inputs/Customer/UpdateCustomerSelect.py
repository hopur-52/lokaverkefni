
class UpdateCustomerInput:

    @staticmethod
    def print_options():
        print("\033[1;44;47m\tPlease choose which customer info to edit: ")
        print("\t 1. Edit Kennitala")
        print("\t 2. Edit Name")
        print("\t 3. Edit Address")
        print("\t 4. Edit Phone")
        print("\t 5. Add/Remove card")
        print("\t 6. Show customer orders")

    @staticmethod
    def choose_option():
        UpdateCustomerInput.print_options()
        inp = input("")
        while inp.lower() not in ['1','2','3','4','5','6','m']:
            print('Invalid selection, please try again.\t\ttype m for menu')
            inp = input()

        if inp == '1':
            return '1'
        elif inp == '2':
            return '2'
        elif inp == '3':
            return '3'
        elif inp == '4':
            return '4'
        elif inp == '5':
            return '5'
        elif inp == '6':
            return 6
        elif inp == 'm':
            return UpdateCustomerInput.choose_option()
