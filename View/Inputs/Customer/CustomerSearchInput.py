from Data.Validators.SearchInputValidator import SearchInputValidator as siv
from View.Exceptions.InvalidInputException import InvalidInputException

class CustomerSearchInput:
    @staticmethod
    def input_field(customer_dict = {}):
        validated = False
        print("\n\033[1;34;40mSearch for customer\n\tKennitala, name or phone (seperated by space, order doesnt matter)\n"
              "\tExamples: '6666661337' or 'Jónas' or 'Jónas Þór'\n")
        while validated == False:
            input_field = input('\tSearch: ')
            check = siv.validate_search_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])

        return CustomerSearchInput.search_all(input_field,customer_dict)

    @staticmethod
    def search_all(search = "", customer_dict = {}):
        search_params = []
        if search == "":
            return customer_dict
        if " " not in search:
            search_params.append(search)
        else:
            search_params = search.split(" ") # Split by space
        search_results = {}

        for w in search_params:
            for key, customer in customer_dict.items():
                if w.lower() in str(customer).lower():
                    search_results[key] = customer

        return search_results
