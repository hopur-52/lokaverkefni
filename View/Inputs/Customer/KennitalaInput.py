from Data.Validators.Customer.KennitalaValidator import KennitalaValidator as kv
from View.Exceptions.InvalidInputException import InvalidInputException

class KennitalaInput:
    @staticmethod
    def input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tKennitala:\t")
            check = kv.validate_kennitala_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])
        return input_field

