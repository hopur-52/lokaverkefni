import unittest

from View.Inputs.Customer.AddressInput import AddressInput

class AddressInputTest(unittest.TestCase):
    def test_address_city_input(self):
        country = AddressInput.country_input_field()
        city = AddressInput.city_input_field()
        street = AddressInput.street_input_field()
        house_number = AddressInput.house_input_field()

