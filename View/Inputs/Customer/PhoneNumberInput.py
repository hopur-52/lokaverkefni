from Data.Validators.Customer.PhoneValidator import PhoneValidator as pv
from View.Exceptions.InvalidInputException import InvalidInputException

class PhoneNumberInput:
    @staticmethod
    def input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tPhone number '5812345': ")
            check = pv.validate_phone_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])
        return input_field

