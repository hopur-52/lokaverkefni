from Data.Validators.Customer.AddressValidator import AddressValidator as av
from View.Exceptions.InvalidInputException import InvalidInputException

class AddressInput:
    @staticmethod
    def city_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tCity: ")
            check = av.validate_address_city_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid city input",check["errors"])
        return input_field

    @staticmethod
    def house_number_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tHouse number: ")
            check = av.validate_address_house_number_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid house number input",check["errors"])
        return input_field

    @staticmethod
    def street_name_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tStreet name: ")
            check = av.validate_address_street_name_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input street name input",check["errors"])
        return input_field

    @staticmethod
    def country_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tCountry: ")
            check = av.validate_address_country_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid country name input",check["errors"])
        return input_field
