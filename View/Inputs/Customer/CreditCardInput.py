from Data.Validators.Customer.CreditCardValidator import CreditCardValidator as ccv
from View.Exceptions.InvalidInputException import InvalidInputException

class CreditCardInput:
    @staticmethod
    def create_card():

        desc = "\033[1;31;40m press 'q' to quit\tpress 'x' to cancel creating credit card\n"
        print("\tCreate credit card\n" + desc)
        card_number = CreditCardInput.card_number_input_field()
        ccv = CreditCardInput.ccv_input_field()
        expire = CreditCardInput.expire_input_field()
        if card_number == '' or ccv == '' or expire == '':
            return ''
        else:
            return '{}///{}///{}'.format(card_number,ccv,expire)


    @staticmethod
    def card_number_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tInput 16 digits in card Number: ")
            CreditCardInput.quit_or_return(input_field)
            check = ccv.validate_CreditCard_input_cardnumber(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid credit card number input",check["errors"])
        return input_field

    @staticmethod
    def ccv_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tInput ccv: ")
            CreditCardInput.quit_or_return(input_field)
            check = ccv.validate_CreditCard_input_ccv(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid ccv input",check["errors"])
        return input_field

    @staticmethod
    def expire_input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tInput expiration date, mm-yy: ")
            CreditCardInput.quit_or_return(input_field)
            check = ccv.validate_CreditCard_input_monthyear(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid expire date format",check["errors"])
        return input_field

    @staticmethod
    def quit_or_return(inp):    # Exit helper
        if inp.lower() == 'q':
            exit()
        if inp.lower() == 'x':
            return ''
