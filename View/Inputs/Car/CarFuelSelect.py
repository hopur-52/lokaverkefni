class CarFuelSelect:
    @staticmethod
    def print_fuel():
        print('\033[1;44;47m\tInsert fuel type: ')
        print('\t 1. 95 Octane')
        print('\t 2. Diesel')
        print('\t 3. Electric')
        print('\t 4. Hybrid')

    @staticmethod
    def select_fuel():
        CarFuelSelect.print_fuel()
        inp = input()
        fuel_type = ''
        while str(inp).lower() not in ['1', '2', '3', '4', 'm']:
            print('Invalid selection, please try again.\t\tType m to see fuel types')
            inp = input()

        if inp == '1':  
            fuel_type = '95 Octane'
            return fuel_type
        elif inp == '2':
            fuel_type = 'Diesel'
            return fuel_type
        elif inp == '3':
            fuel_type = 'Electric'
            return fuel_type
        elif inp == '4':
            fuel_type = 'Hybrid'
            return fuel_type
        elif inp == 'm':
            return CarFuelSelect.select_fuel()
