from Data.Validators.Car.CarBrandValidator import CarBrandValidator as cbv
from View.Exceptions.InvalidInputException import InvalidInputException

class CarBrandInput:
    @staticmethod
    def input_field():
        print("\033[1;44;47m\tPlease input carbrand:")
        validated = False
        while validated == False:
            input_field = input()
            check = cbv.validate_car_brand_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])
        return input_field
