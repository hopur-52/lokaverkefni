from Data.Validators.Car.CarColorValidator import CarColorValidator as cv
from View.Exceptions.InvalidInputException import InvalidInputException

class CarColorInput:
    @staticmethod
    def input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tInput a time in format: 'hh:mm'\n")
            if cv.validate_car_color_input(input_field)['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",cv.validate_car_color_input(input_field)["errors"])
        return input_field

