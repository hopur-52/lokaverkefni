from Data.Validators.Car.LicensePlateValidator import LicensePlateValidator as lpv
from View.Exceptions.InvalidInputException import InvalidInputException

class CarLicensePlaceInput:
    def input_search_license(car_dict):
        validated = False
        while validated == False:
            car_license = input("\033[1;44;47m\tInput car license number: ")
            check = lpv.validate_license_plate_in_dict(car_license,car_dict)
            if check['status'] == "success":
                validated = True
                print("\tFound: \t {}".format(check['car']))
            else:
                assert InvalidInputException("License plate: '{}' not found.".format(car_license),check["errors"])

        return car_license
