from Data.Validators.SearchInputValidator import SearchInputValidator as siv
from View.Exceptions.InvalidInputException import InvalidInputException

class CarSearchInput:
    @staticmethod
    def input_field(car_dict = {}):
        validated = False
        print("\n\033[1;34;40mSearch for car\n\tLicense plate, color, brand, type or fuel type (seperated by space, order doesnt matter)\n"
              "\tExamples: 'Toyota Yaris' or 'Red Skoda' or 'Red Disel'\n")
        while validated == False:
            input_field = input('\tSearch: ')
            check = siv.validate_search_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])

        return CarSearchInput.search_all(input_field,car_dict)

    @staticmethod
    def search_all(search = "", car_dict = {}):
        search_params = []
        if search == "":
            return car_dict
        if " " not in search:
            search_params.append(search)
        else:
            search_params = search.split(" ") # Split by space
        search_results = {}

        for w in search_params:
            for key, car in car_dict.items():
                if w.lower() in str(car).lower():
                    search_results[key] = car

        return search_results
