import unittest

from View.Inputs.Car.CarSearchInput import CarSearchInput
from Data.Models.CarModel import CarModel

class CarSearchInputTest(unittest.TestCase):
    def setUp(self):
        cars = {}
        cars['FD-234'] = CarModel('FD-234', 'Verðflokkur 2', '55000', 'Fólksbíll', 'Honda', 'Grár', 'Disel')
        cars['XY-567'] = CarModel('XY-567', 'Verðflokkur 3', '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Disel')
        cars['FE-537'] = CarModel('FE-537', 'Verðflokkur 1', '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Disel')
        cars['28-527'] = CarModel('28-527', 'Verðflokkur 1', '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Dísel')

        self.cars = cars

    def test_search_car_input(self):
        print(CarSearchInput.input_field(self.cars))
