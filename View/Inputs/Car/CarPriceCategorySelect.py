class CarPriceCategorySelect:
    @staticmethod
    def print_price_categories():
        print('\033[1;44;47m\tPlease select price category: ')
        print('\t 1. Bronze')
        print('\t 2. Silver')
        print('\t 3. Gold')
        print('\t 4. Platinum')

    @staticmethod
    def select_car_price_category():
        CarPriceCategorySelect.print_price_categories()
        inp = input()
        while str(inp).lower() not in ['1', '2', '3', '4', 'm']:
            print('Invalid selection, please try again.\t\tType m to see price categories')
            inp = input()

        if inp == '1':
            price_category = 'Bronze'
            return price_category
        elif inp == '2':
            price_category = 'Silver'
            return price_category
        elif inp == '3':
            price_category = 'Gold'
            return price_category
        elif inp == '4':
            price_category = 'Platinum'
            return price_category
        elif inp == 'm':
            return CarPriceCategorySelect.select_car_price_category()
