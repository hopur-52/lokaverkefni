import unittest
from View.Inputs.Car.CarLicensePlateInput import CarLicensePlaceInput as clp_input
from Data.Models.CarModel import CarModel

class CarLicensePlateTestInput(unittest.TestCase):

    def setUp(self):
        cars = {}
        cars['FD-234'] = CarModel('FD-234', 'Verðflokkur 2', '55000', 'Fólksbíll', 'Honda', 'Grár', 'Bensín')
        cars['XY-567'] = CarModel('XY-567', 'Verðflokkur 3', '69000', 'Jeppi', 'Toyota Cruiser', 'Svartur','Dísel')

        self.cars = cars


    def test_car_license_search(self):
        print(clp_input.input_search_license(self.cars))
