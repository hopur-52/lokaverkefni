class CarTypeSelect:
    @staticmethod
    def print_car_types():
        print('\033[1;44;47m\tPlease select car type: ')
        print('\t 1. Family car')
        print('\t 2. Sportscar')
        print('\t 3. Jeep')
        print('\t 4. SUV')
        print('\t 5. Truck')
        print('\t 6. Motorcycle')

    @staticmethod
    def select_car_type():
        CarTypeInput.print_car_types()
        inp = input()
        while str(inp).lower() not in ['1', '2', '3', '4', '5', '6', 'm']:
            print('Invalid selection, please try again.\t\tType m to see car types')
            inp = input()

        if inp == '1':
            car_type = 'Family car'
            return car_type
        elif inp == '2':
            car_type = 'Sportscar'
            return car_type
        elif inp == '3':
            car_type = 'Jeep'
            return car_type
        elif inp == '4':
            car_type = 'SUV'
            return car_type
        elif inp == '5':
            car_type = 'Truck'
            return car_type
        elif inp == '6':
            car_type = 'Motorcycle'
            return car_type
        elif inp == 'm':
            return CarTypeSelect.select_car_type()
