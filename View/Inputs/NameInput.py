from Data.Validators.NameValidator import NameValidator as nv
from View.Exceptions.InvalidInputException import InvalidInputException

class NameInput:
    @staticmethod
    def input_field():
        validated = False
        while validated == False:
            input_field = input("\033[1;44;47m\tName: ")
            check = nv.validate_name_input(input_field)
            if check['status'] == "success":
                validated = True
            else:
                assert InvalidInputException("Invalid input",check["errors"])
        return input_field

